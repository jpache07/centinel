FROM python:3.6.2-alpine3.6
ARG REQUIREMENTS
RUN apk update
RUN apk add --no-cache \
        postgresql-dev \
        postgresql-client \
        libffi-dev \
        gcc \
        bash \
        zlib-dev \
        jpeg-dev \
        gettext \
        linux-headers\
        libc-dev musl-dev

RUN mkdir /code
ADD ./ /code/
WORKDIR /code
RUN pip3.6 install -r requirements/$REQUIREMENTS