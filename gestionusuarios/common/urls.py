from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from django.views.generic import TemplateView
from .views import *

router = SimpleRouter()
router.register("nacionalidades", NacionalidadViewSet, 'nacionalidades')

urlpatterns = [

	url(r'^', include(router.urls)),
]
