from django.contrib import admin, messages
from .models import *


class BaseAdmin(admin.ModelAdmin):

    actions = ['cambiar_activacion']

    def cambiar_activacion(self, request, queryset):
        model = self.model._meta.verbose_name
        n = queryset.count()
        self.message_user(request,"%(count)d %(model)s cambiadas." % {"count": n ,"model":model}, messages.SUCCESS)   
        for item in queryset:
            if item.condicion:
                item.condicion = False
            else:
                item.condicion = True
            item.save()
        return queryset
    cambiar_activacion.short_description = 'Cambia el estado de la activacion'



@admin.register(Sistema)
class SistemaAdmin(BaseAdmin):
    pass

@admin.register(Nacionalidad)
class NacionalidadAdmin(BaseAdmin):
    pass
