from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
# from django_datatables_view.base_datatable_view import BaseDatatableView,DatatableMixin
# from django_datatables_view.mixins import JSONResponseView,JSONResponseMixin
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth import get_user_model
from rest_framework_jwt.utils import jwt_decode_handler
from rest_framework import status
from django.utils.translation import ugettext_lazy as _

User = get_user_model()

class BaseAPIView(APIView):
    """
    Base API View 
    """

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        return {
            'request': self.request,
            'view': self,
        }

    def get_serializer_class(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.
        You may want to override this if you need to provide different
        serializations depending on the incoming request.
        (Eg. admins get full serialization, others get basic serialization)
        """
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__)
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)


class BasePostAPIView(BaseAPIView):

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            if getattr(data,"id",None):
                resp = {"success": True, "id": data.id}
            else:
                resp = {"success": True}
            return Response(resp, status=status.HTTP_201_CREATED)


class BulkAPIView(BaseAPIView):


    def crear(self, object):
        serializer = self.get_serializer(data=object)
        serializer.is_valid(raise_exception=True)
        data = serializer.save()

    def eliminar(self, object_id):
        
        try:
            instance = self.modelo.objects.get(id=object_id)
            instance.delete()
        except Exception as e:
            print (e)

    def post(self, request, *args, **kwargs):
        import json
        data = json.loads(request.body)
        
        for object in data:
            try:
                object['modificar']
                self.modificar(object)
            except KeyError as e:
                self.eliminar(object['id'])
            except KeyError as e:
                self.crear(object)

        resp = {"success": True}
        return Response(resp, status=status.HTTP_201_CREATED)


class PostSerializerProcessViewMixin():
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user.profile, data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            return Response({"success": True})


class PutSerializerProcessViewMixin():
    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user.profile, data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save()
            return Response({"success": True})

class MultipleChoicesMixins():
    choices_response = {}

    def get_choices_response(self):
        return self.choices_response

    def reformat_object(self, array):
        return [{"id": obj[0], "name": obj[1]} for obj in array]

    def get_data(self):
        return {key: self.reformat_object(value) for key, value in self.get_choices_response().items()}


class MultipleChoicesAPIView(MultipleChoicesMixins, APIView):
    def get(self, request, version, format=None):
        return Response(self.get_data())


class MultipleChoicesViewSet(MultipleChoicesMixins, ModelViewSet):
    @list_route(["get"])
    def form(self, request, **kwargs):
        return Response({key: self.reformat_object(value) for key, value in self.get_choices_response().items()})

# class DataTable(DatatableMixin, JSONResponseMixin, TemplateView):

#     def get(self, request, *args, **kwargs):
#         token = self.request.META.get("HTTP_AUTHORIZATION")
#         datos = jwt_decode_handler(token)
#         request.user = User.objects.get(id=datos['user_id'])
#         self.usuario = request.user
#         return JSONResponseMixin.get(self,request=request,*args,**kwargs)


class BorrarView(object):

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()

        if request.query_params.get('borrar', None):
            instance.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        if instance.condicion:
            instance.condicion = False
        else:
            instance.condicion = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
