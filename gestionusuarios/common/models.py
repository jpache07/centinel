import uuid
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    condicion = models.BooleanField(default=True)

    class Meta:
        abstract = True


class UserByBaseModel(BaseModel):
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False,
                                   related_name="created_%(app_label)s_%(class)s_set",
                                   on_delete=models.CASCADE)
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False,
                                   related_name="modified_%(app_label)s_%(class)s_set",
                                   on_delete=models.CASCADE) 

    class Meta:
        abstract = True


class NameBaseModel(BaseModel):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ("name",)

    def __str__(self):
        return self.name


class UserOneBaseModel(BaseModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.user)


class UserRelation(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        abstract = True

class UserRelationNull(BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             null=True, blank=True)

    class Meta:
        abstract = True


class Nacionalidad(BaseModel):
    nacionalidad = models.CharField(max_length=50)

    def __str__(self):
        return self.nacionalidad

    class Meta:
        verbose_name = 'Nacionalidad'
        verbose_name_plural = 'Nacionalidades'
        ordering = ('nacionalidad', )


class ConfiguracionGeneral(BaseModel):
    """Por ejemplo establecer un nombre para exportar 
    por defecto"""

    clave = models.CharField(max_length=80)
    valor = models.CharField(max_length=500)

    def __str__(self):
        return str(self.clave)


class ConfiguracionPersona(BaseModel):
    """
    Por ejemplo establecer un nombre para exportar 
    solo para esta persona
    """

    clave = models.CharField(max_length=80)
    valor = models.CharField(max_length=500)
    persona = models.ForeignKey('personas.Persona', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.clave)


class Sistema(BaseModel):

    nombre = models.CharField(_("Nombre del Sistema"), max_length=50, null=False)
    uuid = models.UUIDField(default=uuid.uuid4,
                            unique=True, null=False,
                            blank=False, editable=True)


class Equipo(NameBaseModel):
    miembros = models.ManyToManyField("personas.Persona")

    def __str__(self):
        return self.nombre