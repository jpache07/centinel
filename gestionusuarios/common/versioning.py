from rest_framework.versioning import URLPathVersioning


class AppVersioning(URLPathVersioning):
	
    default_version = "v1"
    allowed_versions = ["v1","v2"] #can add new versions here "v1.1"
    version_param = "version"
