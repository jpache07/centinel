from django.conf import settings
from django.contrib.auth.decorators import user_passes_test

def authenticated_redirect(function=None, login_url=settings.LOGIN_REDIRECT_URL):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    actual_decorator = user_passes_test(lambda u: u.is_authenticated() == False, login_url)
    if function:
        return actual_decorator(function)
    return actual_decorator