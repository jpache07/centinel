import hashlib
import random
import requests

from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.core.mail import EmailMessage


def send_mail(subject_template_name, email_template_name,
              context, from_email, to_email, html_email_template_name=None):
    """
    Sends a django.core.mail.EmailMultiAlternatives to `to_email`.
    """

    subject = loader.render_to_string(subject_template_name, context)
    # # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context)


    # email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
    # if html_email_template_name is not None:
    #     html_email = loader.render_to_string(html_email_template_name, context)
    #     email_message.attach_alternative(html_email, 'text/html')


    correo = EmailMessage(subject,body,[to_email])
    correo.send()

def generate_random_token(extra=None, hash_func=hashlib.sha256):
    if extra is None:
        extra = []
    bits = extra + str(random.SystemRandom().getrandbits(512))
    return hash_func("".join(bits).encode("utf-8")).hexdigest()




class ConnectionError(requests.exceptions.RequestException):
    """A Connection error occurred."""      
    pass

def envio_token(url, datos):
    import requests
    from requests.packages.urllib3.util.retry import Retry
    from requests.adapters import HTTPAdapter    
    sessions = requests.Session()
    retry = Retry(connect=2, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    sessions.mount('http://', adapter)
    sessions.mount('https://', adapter)
    try:
        print(url)
        resp = sessions.post(url, data=datos)
        print (resp)
        return resp
    except ConnectionError as e:
        print ('65')
        print(e) 
        return None