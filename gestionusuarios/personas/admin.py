from django.contrib import admin
from .models import *

class PersonaAdmin(admin.ModelAdmin):

    list_display = ('nombre','apellido','tipo',
                    'documento','user','uuid')

    def nombre(self,obj):
        return obj.primer_nombre

    def apellido(self,obj):
        return obj.primer_apellido

    def tipo(self,obj):
        return obj.tipo_documento

    def documento(self,obj):
        return obj.numero_documento

    def user(self,obj):
        return obj.user or None

admin.site.register(Persona,PersonaAdmin)

admin.site.register(TipoTelefono)
admin.site.register(Telefono)
admin.site.register(Correo)
admin.site.register(TipoCorreo)

admin.site.register(ContactoEmergencia)
admin.site.register(Hijo)
admin.site.register(Mascota)

admin.site.register(Direccion)



