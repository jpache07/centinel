import jwt
from django.contrib.auth import get_user_model
from django.utils.encoding import smart_text
from django.utils.translation import ugettext as _
from django.contrib.auth.backends import ModelBackend
from rest_framework import exceptions
from rest_framework.authentication import (BaseAuthentication, get_authorization_header, CSRFCheck)
from rest_framework_jwt.settings import api_settings
from django.shortcuts import redirect
from django.urls import reverse
#from social_core.pipeline.partial import partial


jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
User = get_user_model()

class UsernameAuthentication(BaseAuthentication):

    def authenticate(self, request):

        username = request.META.get('HTTP_AUTHORIZATION',None)
        if username is None:
            return

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            msg = _('No se encontro usuario')
            raise exceptions.AuthenticationFailed(msg)

        return (user, None)


class EmailAuthBackend(ModelBackend):
    """Allow users to log in with their email address"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        # Some authenticators expect to authenticate by 'username'
        email = username
        if email is None:
            email = kwargs.get('username')

        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                user.backend = "%s.%s" % (self.__module__, self.__class__.__name__)
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class MobileAuthBackend(ModelBackend):
    """Allow users to log in with their mobile phone"""

    def authenticate(self, request, username=None, password=None, **kwargs):
        # Some authenticators expect to authenticate by 'username'
        mobile = username
        if mobile is None:
            mobile = kwargs.get('username')

        try:
            user = User.objects.get(persona__mobile=mobile)
            if user.check_password(password):
                user.backend = "%s.%s" % (self.__module__, self.__class__.__name__)
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

# class DevelopmentSessionAuthentication(BaseAuthentication):
#     """
#     Use Django's session framework for authentication.
#     """

#     def authenticate(self, request):
#         """
#         Returns a `User` if the request session currently has a logged in user.
#         Otherwise returns `None`.
#         """

#         # Get the session-based user from the underlying HttpRequest object
#         user = getattr(request._request, 'user', None)

#         # Unauthenticated, CSRF validation not required
#         if not user or not user.is_active:
#             return None

#         jwttoken = JSONWebTokenAuthentication().get_jwt_value(request)

#         if not jwttoken:
#             self.enforce_csrf(request)

#         # CSRF passed with authenticated user
#         return (user, None)

#     def enforce_csrf(self, request):
#         """
#         Enforce CSRF validation for session based authentication.
#         """
#         reason = CSRFCheck().process_view(request, None, (), {})
#         if reason:
#             # CSRF failed, bail with explicit error message
#             raise exceptions.PermissionDenied('CSRF or JWT Failed: %s' % reason)


class SertivenAuthentication(BaseAuthentication):
    """
    Token based authentication using the JSON Web Token standard.
    Token de otro sistema, tambien autentica
    """

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        
        try:
            
            token = request.META['HTTP_X_SERTIVEN_AUTH'][4:]
            decode = jwt_decode_handler(token)
            user = User.objects.get( username=decode['username'])

            return (user, token)
        except Exception as e:
            #print ("except en SertivenAuthentication")
            #print (e)
            return None

# @partial
# def require_email(strategy, details, response, user=None,
#                   is_new=False, *args, **kwargs):
#     if user and user.email:
#         return
#     elif is_new and not details.get('email'):
#         email = strategy.request_data().get('email')
#         if email:
#             details['email'] = email
#         else:
#             return redirect(reverse("account:register"))
