# Generated by Django 2.0 on 2018-10-05 18:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('personas', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='direccion',
            old_name='address_line',
            new_name='linea_direccion',
        ),
    ]
