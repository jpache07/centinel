from django.conf.urls import url, include
from django.contrib.auth.views import LoginView
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token
from .views import *

app_name = 'personas'

router = SimpleRouter()
router.register("tipos_telefonos", TipoTelefonosViewSet,'tipos de telefonos')
router.register("tipos_correos", TipoCorreosViewSet,'tipos de correos')
router.register("persona", PersonaViewSet,'personas')

urlpatterns = [

    #""" URLS de token """
    url(r'^auth/verify/$', verify_jwt_token),
    url(r'^auth/refresh/$', refresh_jwt_token),
    url(r'^auth/ligero/$', obtain_jwt_token, name="token"),
    url(r'^auth/$', ObtainJSONWebToken.as_view(), name="login"),


    #""" URLS de claves """
    url(r'^auth/password_reset_send_email/$',
        PasswordResetSendEmail.as_view(),
        name="password_reset_send_email"),

    url(r'^auth/password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordVerifyToken.as_view(),
        name="password_reset_verify"),

    url(r'^auth/password_reset/$',
        PasswordReset.as_view(),
        name="password_reset"),

    url(r'^auth/cambio_clave/$', CambioClave.as_view(), name="cambio_clave"),


    #""" URLS de usuario """
    url(r'^user/$', UserView.as_view(), name="user"),
    url(r'^users/form/$',UserFormChoicesList.as_view(),
        name="user-form-choices"),
    url(r'^users/validaciones/$', ValidacionesViewSet.as_view(),
        name='validaciones_usuario'),
    url(r'^users/(?P<pk>\d+)/$', UserViewSet.as_view(),
        name="user-detail"),


    #"""URLS de telefonos"""
    url(r'^users/telefonos/principal/(?P<uuid>[-\w]+)/$',
        TelefonoPrincipalViewSet.as_view(),
        name='telefono-principal'),
    url(r'^users/telefonos/todos/(?P<uuid>[-\w]+)/$',
        TelefonosPersonaViewSet.as_view(),
        name='telefonos-persona'),
    url(r'^users/telefonos/bulk/(?P<uuid>[-\w]+)/$',
        TelefonosPersonaBulkViewSet.as_view(),
        name='telefonos-persona-bulk'),


    #"""URLS de correos"""
    url(r'^users/correos/todos/(?P<uuid>[-\w]+)/$',
        CorreosPersonaViewSet.as_view(),
        name='correos-persona'),
    url(r'^users/correos/bulk/(?P<uuid>[-\w]+)/$',
        CorreosPersonaBulkViewSet.as_view(),
        name='correos-persona-bulk'),

    #"""URLS de persona"""

    url(r'^users/persona/buscar/(?P<tipo>\w+)/(?P<documento>\w+)/$', BuscarPorDocumentoViewSet.as_view(),
        name='buscar_persona'),
    url(r'^users/persona/asociar$', AsociarUsuarioPersonaViewSet.as_view(),
        name="user_persona"),
    #url(r'^users/personas/', persona),
    ### regresa el uuid de un token
    url(r'^users/persona_por_jwt/$', PersonaPorJwtViewSet.as_view(),
        name='persona-por-jwt'),

    url(r'^users/persona/avatar$', AvatarView.as_view(), name="avatar-persona"),
    url(r'^users/persona/avatar/(?P<uuid>[-\w]+)/$', AvatarDetailView.as_view(), name="avatar-persona-uuid"),

    url(r'^documentos', DocumentChoicesList.as_view()),

    url(r'^', include(router.urls)),

]
