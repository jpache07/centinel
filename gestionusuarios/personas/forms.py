from datetime import date, timedelta
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.forms import AuthenticationForm as authform
from django.contrib.auth.forms import PasswordResetForm as pwdform, UsernameField
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.validators import RegexValidator
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext as _
from .models import Persona

class PasswordResetForm(pwdform):
    email = forms.EmailField(label="Email", max_length=254,
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))

    def save(self, domain_override = None,
             subject_template_name = 'registration/password_reset_subject.txt',
             email_template_name = 'registration/password_reset_email.html',
             use_https = False, token_generator = default_token_generator,
             from_email = None, request = None, html_email_template_name = None,
             extra_email_context = None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        email = self.cleaned_data["email"]
        users = list(self.get_users(email))

        current_site = get_current_site(request)
        site_name = settings.SITE_NAME
        domain = current_site.domain

        if users:
            for user in users:

                context = {
                    'email': user.email,
                    'domain': domain,
                    'site_name': site_name,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'user': user,
                    'token': token_generator.make_token(user),
                    'protocol': 'https' if use_https else 'http',
                }
                if extra_email_context is not None:
                    context.update(extra_email_context)
                self.send_mail(
                    subject_template_name, email_template_name, context, from_email,
                    user.email, html_email_template_name=html_email_template_name,
                )
        else:
            context = {
                'email': email,
                'domain': domain,
                'site_name': site_name,
                'protocol': 'https' if use_https else 'http',
            }
            self.send_mail("accounts/password_reset_email_noexists_subject.txt",
                           "accounts/password_reset_email_noexists.txt", context, from_email,
                           email, html_email_template_name="accounts/password_reset_email_noexists.html", )

class AuthenticationForm(authform):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
        help_text=_("EMAIL, PHONE, OR USERNAME")
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=_("Password")
    )

    class Meta:
        help_texts = {
            'username': _("EMAIL, PHONE, OR USERNAME"),
        }

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                             message=_("The mobile number must be entered in the format: '+999999999' and a "
                                       "maximum of 15 digits is allowed."))

class RegisterUserForm(forms.ModelForm):
    phone = forms.CharField(validators=[phone_regex])
    password = forms.CharField(widget=forms.PasswordInput(),
                               required=False)
    facebook_access_token = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = get_user_model()
        fields = [
            'first_name', 'last_name', 'email'
        ]

    def clean_password(self):
        password = self.cleaned_data.get('password')
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(password, self.instance)
        return password

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if email and get_user_model().objects.filter(email=email).count():
            raise forms.ValidationError(_('This email address is already in use.'))
        return email

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        if phone and get_user_model().objects.filter(Persona__mobile=phone).count():
            raise forms.ValidationError(_('This mobile number is already registered.'))
        return phone

class PasswordResetMobileUserForm(forms.Form):
    """mobile reset password form, to send the code to the user to recover his password """
    mobile = forms.CharField(validators=[phone_regex])


    def clean_mobile(self):
        mobile = self.cleaned_data.get('mobile')
        if not mobile or get_user_model().objects.filter(persona__mobile=mobile).count() == 0:
            raise forms.ValidationError(_('This mobile number is not registered.'))
        return mobile

class BirthdayForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ["nacimiento", ]

    def __init__(self, *args, **kwargs):
        super(BirthdayForm, self).__init__(*args, **kwargs)
        self.fields["nacimiento"].required = True

    def clean_birth_date(self):
        dob = self.cleaned_data['nacimiento']
        today = date.today()

        if (dob.year + 13, dob.month, dob.day) > (today.year, today.month, today.day):
            raise forms.ValidationError(
                _('We are unable to process this registration. You are ineligible to register for Asante.'))
        return dob

class MobileVerificationForm(forms.Form):
    code = forms.CharField(max_length=6)

class MobileResetPasswordForm(forms.Form):
    """Done mobile reset password form, to check the code send to the user its valid."""
    code = forms.CharField(max_length=6)

    def clean_code(self):
        code = self.cleaned_data.get('code')
        if not code or get_user_model().objects.filter(
                persona__mobile_verification_number=code,
                persona__mobile_restored_time__gte=timezone.now() - timedelta(hours=1)).count() == 0:
            raise forms.ValidationError(_('Invalid verification code.'))
        return code
