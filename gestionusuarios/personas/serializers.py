from datetime import date
from random import random

from django.conf import settings
from django.contrib.auth import get_user_model, password_validation, authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions
from django.db import IntegrityError
from django.db.models import OneToOneField
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.text import slugify
from django.utils.translation import ugettext as _
from django.core.mail import send_mail
from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework import serializers
from rest_framework.exceptions import NotFound
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import HyperlinkedModelSerializer, raise_errors_on_nested_writes
from rest_framework.utils import model_meta
from rest_framework.validators import UniqueValidator
from rest_framework_jwt.settings import api_settings
from rest_framework.fields import get_error_detail
from rest_framework.validators import UniqueTogetherValidator
from rest_framework_jwt.compat import Serializer as SerializerJWT, PasswordField ,\
    get_username_field


from .models import user_directory_path, Persona, Telefono, TipoTelefono, Correo, TipoCorreo
import os

from personas.forms import phone_regex
from common.fields import ParameterisedHyperlinkedIdentityField
from common.serializers import QueryFieldsMixin
from common.models import Nacionalidad

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


# avatar
# try:
#     from cStringIO import StringIO
# except ImportError:
#     from io import StringIO
# try:
#     from PIL import Image
# except ImportError:
#     import Image
# from django.core.files import File
# AVATAR_CROP_MAX_SIZE = getattr(settings, 'AVATAR_CROP_MAX_SIZE', 450)
# AVATAR_CROP_MIN_SIZE = getattr(settings, 'AVATAR_CROP_MIN_SIZE', 49)


class CreateUserSerializer(serializers.Serializer):

    date_joined = serializers.DateTimeField(
        read_only=True, default=timezone.now)
    username = serializers.CharField(required=True)
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)
    id = serializers.IntegerField(read_only=True)
    roles = SerializerMethodField(read_only=True)
    user_fields = ["email"]

    def get_roles(self, obj):
        user = self.context['request'].user
        roles = []
       
        return roles

    def validate(self, attrs):
        # here data has all the fields which have validated values
        # so we can create a User instance out of it
        user_data = {your_key: attrs[your_key]
                     for your_key in self.user_fields}

        user = User(**user_data)

        # get the password from the data
        password = attrs.get('password')

        errors = dict()
        try:
            # validate the password and catch the exception
            password_validation.validate_password(password=password, user=user)

        # the exception raised here is different than
        # serializers.ValidationError
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        return super(CreateUserSerializer, self).validate(attrs)

    def create(self, validated_data):

        user_data = {your_key: validated_data[
            your_key] for your_key in self.user_fields}

        user = User(**user_data)
        user.username = validated_data["username"]

        if User.objects.filter(username=user.username).exists():
            user.username = slugify(
                "{}-{}".format(validated_data["username"], int(random() * 1000)))

        user.set_password(validated_data['password'])
        user.save()

        user = authenticate(username=user.username,
                            password=validated_data['password'])

        return user


class UpdateUserSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    username = serializers.CharField(validators=[UniqueValidator(queryset=User.objects.all())],
                                     required=False)
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())],
                                   required=False)

    def update(self, instance, validated_data):

        if validated_data.get('username', None):
            instance.username = validated_data.get('username')

        if validated_data.get('email', None):
            instance.email = validated_data.get('email')

        instance.save()
        return instance


    class Meta:
        model = User
        fields = ('username', 'email',)


class UserDetailSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    persona = serializers.IntegerField(source="persona_set.id")

    class Meta:
        model = User
        fields = ('username', 'persona', 'email')


class PasswordResetSendEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs.get('email')
        errors = dict()

        try:
            user = User.objects.get(
                email__iexact=email,
                is_active=True)

            attrs['user'] = user
        except User.DoesNotExist:
            errors['email'] = _('El correo no existe.')

        if errors:
            raise serializers.ValidationError(errors)

        return super(PasswordResetSendEmailSerializer, self).validate(attrs)

    def get_context_email(self, request, validated_data):
        email = validated_data.get('email')
        user = validated_data.get('user')
        current_site = get_current_site(request)
        site_name = current_site.name
        token = default_token_generator.make_token(user)
        domain = current_site.domain
        uid = urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        use_https = request.is_secure()
        reset_url = settings.PASSWORD_RESET_VERIFY_URL % (str(uid[0]), token)
        context = {
            'email': email,
            'domain': domain,
            'site_name': site_name,
            'reset_url': reset_url,
            'user': user,
            'protocol': 'https' if use_https else 'http',
        }
        return context

    def create(self, validated_data):
        subject_template_name = 'password_reset_subject.txt',
        template_name_html = 'password_reset_email.html',
        template_name_txt = 'password_reset_email.txt',
        email = validated_data.get('email', None)
        from django.template import loader
        context = self.get_context_email(
            self.context["request"], validated_data)
        subject = loader.render_to_string(subject_template_name, context)
        # # Email subject *must not* contain newlines
        #subject = ''.join(subject[0].splitlines())
        body = loader.render_to_string(template_name_txt, context)
        body_html = loader.render_to_string(template_name_html, context)
        return "{protocol}://{domain}{reset_url}".format(
                protocol=context['protocol'],domain=context['domain'],reset_url=context['reset_url'])
        send_mail(subject, body, 'jovan@mail.com', [email],
                  fail_silently=False, auth_user=None, auth_password=None,
                  connection=None, html_message=body_html)

        return validated_data


class PasswordVerifyTokenSerializer(serializers.Serializer):
    token = serializers.CharField()
    uidb64 = serializers.CharField()

    def validate(self, attrs):

        token = attrs.get('token')
        uidb64 = attrs.get('uidb64')
        errors = dict()

        user = self.get_user(uidb64)
        if user is not None:
            if not self.verify_token(user, token):
                errors['token'] = _('token not verify.')
        else:
            errors['uidb64'] = _('Not user get.')

        if errors:
            raise serializers.ValidationError(errors)

        return super(PasswordVerifyTokenSerializer, self).validate(attrs)

    def get_user(self, uidb64):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def verify_token(self, user, token):
        if default_token_generator.check_token(user, token):
            return True
        else:
            return False

    def create(self, validated_data):
        return validated_data


class PasswordResetSerializer(serializers.Serializer):
    new_password1 = serializers.CharField()
    new_password2 = serializers.CharField()
    uidb64 = serializers.CharField()

    def get_user(self, uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def validate(self, attrs):
        uidb64 = attrs.get('uidb64')
        new_password1 = attrs.get('new_password1')
        new_password2 = attrs.get('new_password2')
        errors = dict()

        if new_password1 is None:
            errors['new_password1'] = _('new_password1 is required.')

        if new_password2 is None:
            errors['new_password2'] = _('new_password2 is required.')

        user = None
        try:
            user = self.get_user(uidb64)
        except IndexError:
            errors['uidb64'] = _('token invalid format.')

        form_data = {
            'new_password1': new_password1,
            'new_password2': new_password2,
        }
        if user:
            form = SetPasswordForm(user, data=form_data)

            if form.errors:
                errors.update(form.errors)
            attrs['form'] = form

        if errors:
            raise serializers.ValidationError(errors)

        return super(PasswordResetSerializer, self).validate(attrs)

    def create(self, validated_data):
        form = validated_data.get('form')
        form.save()
        return validated_data


class CambioClaveSerializer(serializers.Serializer):
    new_password1 = serializers.CharField()
    new_password2 = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        new_password1 = attrs.get('new_password1')
        new_password2 = attrs.get('new_password2')
        password = attrs.get('password')
        errors = dict()

        if password is None:
            errors['password'] = _('password is required.')

        if new_password1 is None:
            errors['new_password1'] = _('new_password1 is required.')

        if new_password2 is None:
            errors['new_password2'] = _('new_password2 is required.')

        user = None
        try:
            user = self.context['request'].user
        except IndexError:
            errors['uidb64'] = _('token invalid format.')

        form_data = {
            'new_password1': new_password1,
            'new_password2': new_password2,
        }

        if user:
            form = SetPasswordForm(user, data=form_data)

            if form.errors:
                errors.update(form.errors)
            attrs['form'] = form

            clave_actual_session = authenticate(
                username=user.username,
                password=password
            )
            print (user)

            if not isinstance(clave_actual_session, User):
                errors['password'] = _('password incorrepto.')

        if errors:
            raise serializers.ValidationError(errors)

        return super(CambioClaveSerializer, self).validate(attrs)

    def create(self, validated_data):
        form = validated_data.get('form')
        form.save()
        return validated_data


class PersonaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    primer_nombre = serializers.CharField(max_length=30, required=True)
    primer_apellido = serializers.CharField(max_length=150, required=True)
    segundo_nombre = serializers.CharField(max_length=150, required=False)
    segundo_apellido = serializers.CharField(max_length=150, required=False)
    tipo_documento = serializers.CharField(max_length=30, required=False, default="cedula")
    numero_documento = serializers.CharField(max_length=30, required=False)
    genero = serializers.BooleanField(required=False)
    nacimiento = serializers.DateField(required=False, format="%d/%m/%Y")
    avatar = serializers.CharField(required=False)
    nacionalidad = serializers.IntegerField(required=False,
                                            write_only=True,
                                            default=1)
    nacionalidad_actual = SerializerMethodField()
    token = SerializerMethodField()
    nacionalidad_actual_texto = serializers.CharField(read_only=True,
                                    source="nacionalidad.nacionalidad")
    direccion = serializers.CharField(max_length=200, required=False)
    username = serializers.CharField(source="user.username", required=False)
    password = serializers.CharField(source="user.password",
                                     write_only=True,
                                     required=False)
    email = serializers.EmailField(source="user.email",required=False,
        validators=[UniqueValidator(queryset=User.objects.all())])
    grupo = serializers.CharField(max_length=200, required=False)

    def get_token(self, obj):
        payload = jwt_payload_handler(obj.user)
        return jwt_encode_handler(payload)

    def get_nacionalidad_actual(self, obj):
        if obj.nacionalidad:
            return obj.nacionalidad.id
        return None

    def validate_nacionalidad(self, id):
        nacionalidad = get_object_or_404(Nacionalidad, pk=id)
        return nacionalidad

    def validate(self, attrs):
        errors = dict()
        if attrs.get('user',None):
            password = attrs.get('user')['password']
            user_data = {
                "email": attrs['user']["email"]
            }
            user = get_user_model()(**user_data)
            try:
                password_validation.validate_password(password=password, user=user)
            except exceptions.ValidationError as e:
                errors = dict()
                errors['password'] = list(e.messages)
                raise serializers.ValidationError(errors)

        if errors:
            raise serializers.ValidationError(errors)

        return super(PersonaSerializer, self).validate(attrs)

    def create_user(self, validated_data):

        user= validated_data.get("user",None)
        if not user:
            return

        username = validated_data.get("username", None)
        email = validated_data["user"]["email"]
        password = validated_data["user"].get("password", None)


        if not username:         
            username = "{}-{}".format(
                        validated_data.get("primer_nombre"),
                        validated_data.get("primer_apellido")
                        )

        user_data = {
            "email": email,
            "username": username
        }

        user = get_user_model()(**user_data)

        if get_user_model().objects.filter(username=user.username).exists():
            user.username = slugify(
                "{}-{}".format(username, int(random() * 1000)))

        if password:
            user.set_password(password)

        if validated_data.get('grupo',None):
            grupo = Group 

        user.save()
        return user

    def create_persona(self, validated_data, user=None):

        nacimiento = validated_data.get("nacimiento", None)
        tipo_documento = validated_data.get("tipo_documento", None)
        numero_documento = validated_data.get("numero_documento", None)
        primer_nombre = validated_data.get("primer_nombre", None)
        primer_apellido = validated_data.get("primer_apellido", None)
        nacionalidad = validated_data.get("nacionalidad", None)
        genero = validated_data.get("genero", None)

        persona = Persona()
        persona.tipo_documento = tipo_documento
        persona.numero_documento = numero_documento
        
        if nacimiento:
            persona.nacimiento = nacimiento

        if user:
            persona.user = user

        persona.nacionalidad = nacionalidad
        persona.genero = genero
        persona.primer_nombre = primer_nombre
        persona.primer_apellido = primer_apellido

        if validated_data.get("segundo_nombre", None):
            persona.segundo_nombre = validated_data.get("segundo_nombre")

        if validated_data.get("segundo_apellido", None):
            persona.segundo_apellido = validated_data.get("segundo_apellido")
        # user.persona.send_email(get_current_site(self.context["request"]).domain,
        #                         'https' if self.context["request"].is_secure() else "http")

        persona.save()
        return persona

    def create(self, validated_data):
        usuario = self.create_user(validated_data)
        retorno = self.create_persona(validated_data, usuario)
        return retorno


    def update(self, instance, validated_data):
        """ update solo es para crearle un usuario"""
        usuario = self.create_user(validated_data)
        instance.user = usuario
        instance.save()
        return instance

    class Meta:
        model = Persona
        fields = (
            'uuid','nacimiento', 'genero', 'primer_nombre', 'segundo_nombre', 'email',
            'primer_apellido', 'segundo_apellido', 'username', 'avatar', 'password',
            'tipo_documento', 'numero_documento', 'nacionalidad', 'direccion',
            'nacionalidad_actual', 'nacionalidad_actual_texto', 'id', 'grupo',
            'token')

        extra_kwargs = {"nacionalidad": {"error_messages": {
            "invalid": "Debe seleccionar un pais"}}}


class ActualizarPersonaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    primer_nombre = serializers.CharField(max_length=30, required=True)
    primer_apellido = serializers.CharField(max_length=150, required=True)
    segundo_nombre = serializers.CharField(max_length=150, required=False)
    segundo_apellido = serializers.CharField(max_length=150, required=False)
    tipo_documento = serializers.CharField(max_length=30, required=False, default="cedula")
    numero_documento = serializers.CharField(max_length=30, required=False)
    genero = serializers.BooleanField(required=False)
    nacimiento = serializers.DateField(required=False, format="%d/%m/%Y",
                                       input_formats=["%d/%m/%Y"])
    
    nacionalidad = serializers.IntegerField(required=False,
                                            write_only=True,
                                            default=1)
    nacionalidad_id = serializers.IntegerField(required=False,
                                            read_only=True, source="nacionalidad.id")
    nacionalidad_nuevo = serializers.CharField(required=False,
                                            read_only=True, source="nacionalidad.nacionalidad")
    direccion = serializers.CharField(max_length=200, required=False)

    def validate_nacionalidad(self, id):
        nacionalidad = get_object_or_404(Nacionalidad, pk=id)
        return nacionalidad

    # def validate(self, attrs):
    #     errors = dict()

    #     if errors:
    #         raise serializers.ValidationError(errors)

    #     return super(ActualizarPersonaSerializer, self).validate(attrs)

    class Meta:
        model = Persona
        fields = (
            'uuid','nacimiento', 'genero', 'primer_nombre', 'segundo_nombre',
            'primer_apellido', 'segundo_apellido','tipo_documento',
            'numero_documento', 'nacionalidad', 'nacionalidad_id', 'nacionalidad_nuevo', 'direccion', 'id')

        extra_kwargs = {"nacionalidad": {"error_messages": {
            "invalid": "Debe seleccionar un pais"}}}


# class AvatarCreateSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

#     x1 = serializers.IntegerField(required=True, write_only=True)
#     x2 = serializers.IntegerField(required=True, write_only=True)
#     y1 = serializers.IntegerField(required=True, write_only=True)
#     y2 = serializers.IntegerField(required=True, write_only=True)

#     def create(self, validated_data):
#         persona = self.context["request"].user.persona
#         persona.avatar = validated_data.get('avatar')
#         persona.save()
#         image = Image.open(persona.avatar)

#         left = validated_data.get('x1')
#         top = validated_data.get('y1')
#         bottom = validated_data.get('y2')
#         right = validated_data.get('x2')

#         # print (validated_data)
#         # box = [ left, top, right, bottom ]
#         # print (box)
#         # result = 'width'
#         # (w, h) = image.size
#         # if result=="width":
#         #     box = map(lambda x: x*h/AVATAR_CROP_MAX_SIZE, box)
#         # else:
#         #     box = map(lambda x: x*w/AVATAR_CROP_MAX_SIZE, box)

#         box = [left, top, right, bottom]
#         image = image.crop(box)
#         if image.mode != 'RGB':
#             image = image.convert('RGB')

#         new_location = "media/" + \
#             user_directory_path(persona, 'avatar_tmp.jpg')
#         image.save(new_location)
#         image.close()
#         # si se descomenta borra el archivo original
#         # persona.avatar.delete()
#         persona.avatar.save('avatar.jpg', open(new_location, 'rb'))
#         os.remove(new_location)
#         return validated_data

#     class Meta:
#         model = Persona
#         fields = ('avatar', 'x1', 'x2', 'y1', 'y2')


class ValidacionesSerializer(serializers.Serializer):
    password = serializers.CharField(required=False)
    username = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)

    def validate(self, attrs):

        self.errores = []
        password = attrs.get('password', None)
        username = attrs.get('username', None)
        email = attrs.get('email', None)

        if not password:
            self.errores.append('La clave es obligatoria')

        if not username:
            self.errores.append('El nombre de usuario es obligatorio')

        if not email:
            self.errores.append('El email es obligatorio')

        try:
            User.objects.get(username=username)
            self.errores.append('El nombre de usuario ya existe')
        except Exception as e:
            pass

        try:
            User.objects.get(email=email)
            self.errores.append('El correo ya existe')
        except Exception as e:
            pass

        data_user = {
            'username': username,
            'email': email
        }
        u = User(**data_user)

        try:
            password_validation.validate_password(password=password, user=u)
        except exceptions.ValidationError as e:
            for err in e.messages:
                self.errores.append(err)

        if self.errores:
            raise serializers.ValidationError(self.errores)
        return super(ValidacionesSerializer, self).validate(attrs)

    def create(self, validated_data):
        return validated_data


class BuscarPorDocumentoSerializer(serializers.Serializer):
    tipo = serializers.CharField(max_length=30)
    documento = serializers.CharField(max_length=30)

    def create(self, validated_data):
        tipo = validated_data.get('tipo', None)
        num = validated_data.get('documento', None)
        persona = get_object_or_404(tipo_documento=tipo, numero_documento=num)
        
        return persona


class AsociarUsuarioPersonaSerializer(serializers.Serializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    persona = serializers.IntegerField(write_only=True)

    def validate(self, attrs):

        errores = {}
        persona = attrs.get('persona', None)

        try:
            Persona.objects.get(id=persona)
        except Exception as e:
            print (e)
            errores['persona'] = 'No existe la persona'

        if errores:
            raise serializers.ValidationError(errores)
        return super(AsociarUsuarioPersonaSerializer, self).validate(attrs)   

    def create(self, validated_data):
        
        user = validated_data.get('user')
        id = validated_data.pop('persona')

        persona = Persona.objects.get(pk=id)
        persona.user = user
        persona.save()

        return validated_data


class TipoTelefonosSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    
    class Meta:
        model = TipoTelefono
        fields = ('id', "tipo_telefono",)


class TipoCorreosSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    
    class Meta:
        model = TipoCorreo
        fields = ('id', "tipo_correo",)


class TelefonoPrincipalPersonaSerializer(QueryFieldsMixin,
                                         HyperlinkedModelSerializer):
    
    principal = serializers.BooleanField(default=True)

    def update(self, instance, validated_data):
        """ update solo es para crearle un usuario"""
        principal = validated_data.pop('principal')
        #Tele update()
        instance.user = usuario
        instance.save()
        return instance

    class Meta:
        model = Telefono
        fields = ('id', 'principal',)
        validators = [
            UniqueTogetherValidator(
                queryset=Telefono.objects.all(),
                fields=('telefono', 'persona'),
                message='Ya posee este numero de telefono'
            )
        ]


class TelefonosPersonaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    tipo_telefono_id = serializers.IntegerField(write_only=True)
    tipo_telefono = serializers.IntegerField(read_only=True,source="tipo_telefono.id")
    tipo_telefono_texto = serializers.CharField(read_only=True,source="tipo_telefono.tipo_telefono")
    persona = serializers.IntegerField(source='persona.id',
                                       read_only=True)
    telefono = serializers.CharField()

    def validate(self, attrs):
        errors = dict()
        uuid = self.context['view'].kwargs['uuid']

        try:
            per = Persona.objects.get(uuid=uuid)
            attrs['persona'] = per
        except Persona.DoesNotExist:
            errors['persona'] = _('No existe la persona')

        try:
            Telefono.objects.get(telefono=attrs['telefono'],
                                 persona=per)
            errors['telefono'] = 'Ya posee este numero de telefono %s' % attrs['telefono']
        except Telefono.DoesNotExist:
            pass 

        if errors:
            raise serializers.ValidationError(errors)

        return super(TelefonosPersonaSerializer, self).validate(attrs)

    def create(self, validated_data):

        tipo_telefono_id = validated_data.pop('tipo_telefono_id')
        persona = validated_data.pop('persona')

        instance = Telefono.objects.create(
                   tipo_telefono_id = tipo_telefono_id,
                   persona=persona,
                   **validated_data)
        return validated_data 

    class Meta:
        model = Telefono
        fields = ('id', "tipo_telefono", "telefono", 'principal','persona',
                  'tipo_telefono_texto','tipo_telefono_id')


class CorreosPersonaSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    persona = serializers.IntegerField(write_only=True)
    tipo_correo = serializers.IntegerField(read_only=True,source="tipo_correo.id")
    tipo_correo_texto = serializers.CharField(source='tipo_correo.tipo_correo',
                                              read_only=True)
    tipo_correo_id = serializers.IntegerField(write_only=True)

    def create(self, validated_data):

        tipo_correo_id = validated_data.pop('tipo_correo_id')
        persona = validated_data.pop('persona')
        instance = Correo.objects.create(
                   tipo_correo_id = tipo_correo_id,
                   persona_id = persona,
                   **validated_data)
        return validated_data 

    class Meta:
        model = Correo
        fields = ('id', "tipo_correo", "correo", 'persona',
                  'tipo_correo_texto', "tipo_correo_id")

        validators = [
            UniqueTogetherValidator(
                queryset=Correo.objects.all(),
                fields=('correo', 'persona'),
                message='Ya posee este correo.'
            )
        ]


class PersonaPorJwtSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    
    class Meta:
        model = Persona
        fields = ('id', "uuid",)


class JSONWebTokenSerializer(SerializerJWT):
    """
    Serializer class used to validate a username and password.

    'username' is identified by the custom UserModel.USERNAME_FIELD.

    Returns a JSON Web Token that can be used to authenticate later calls.
    """

    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(JSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField()
        self.fields['password'] = PasswordField(write_only=True)

    @property
    def username_field(self):
        return get_username_field()

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password')
        }

        if all(credentials.values()):
            user = authenticate(**credentials)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user)
                persona = Persona.objects.get(user=user)
                #telefono = list(Telefono.objects.filter(persona_id=persona.id).values('id','telefono','tipo_telefono','principal'))
                #correo = list(Correo.objects.filter(persona_id=persona.id).values('id','correo','tipo_correo'))            
                return {
                    'token': jwt_encode_handler(payload),
                    'user': user,
                    'persona': persona,
                    #'telefono': telefono,
                    #'correo': correo,
                    'exp':str(payload['exp'])
                }
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)
