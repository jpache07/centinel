import random
import uuid as uuid_lib

from datetime import timedelta
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from common.fields import PaymentIdField
from common.models import UserOneBaseModel, BaseModel, Nacionalidad, NameBaseModel
from common.utils import send_mail, generate_random_token

SEXO_CHOICES = ((True, _('Masculino')), (False, _('Femenino')))

TIPO_SANGRE_CHOICES = (
    ("A+", "A+"),
    ("A-", "A-"),
    ("B+", "B+"),
    ("B-", "B-"),
    ("AB+", "AB+"),
    ("AB-", "AB-"),
    ("O+", "0+"),
    ("O-", "O-"),
)

DOCUMENT_CHOICES = (
    ("dni", _('Documento nacional de identificacion')),
    ("cedula", _('Cedula')),
    ("rif", _('Rif')),    
    ("pasaporte", _('Pasaporte')),  
    ("cuit", _('cuit')),  
    ("ric", _('Registro de identidad civil')),
)


def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class Persona(UserOneBaseModel):

    #datos personales
    primer_nombre = models.CharField(_("Nombre"), max_length=30)
    segundo_nombre = models.CharField(_("Segundo Nombre"), max_length=30,
                                      null=True, blank=True)
    primer_apellido = models.CharField(_("Apellido"), max_length=100,
                                       null=False)
    segundo_apellido = models.CharField(_("Segundo Apellido"), max_length=100,
                                        null=True, blank=True)
    nacimiento = models.DateField(null=True, blank=True)
    sexo = models.NullBooleanField(_("Genero"), choices=SEXO_CHOICES, default=None)

    #usuario
    user = models.OneToOneField(settings.AUTH_USER_MODEL, blank=True,
                                null=True,  on_delete=models.PROTECT,
                                related_name="persona_set")

    #legales
    tipo_documento = models.CharField(_("Tipo de documento"), null=True, blank=True,
                                        max_length=10, choices=DOCUMENT_CHOICES)
    numero_documento = models.CharField(_("Numero de documento"), null=True, blank=True, max_length=30)
    nacionalidad = models.ForeignKey(Nacionalidad, null=True, blank=True, on_delete=models.PROTECT)
    direccion = models.ForeignKey("Direccion", null=True, blank=True, on_delete=models.CASCADE)
    verificado = models.NullBooleanField(_("Verificado"), default=None)

    #avatar 
    avatar = models.ImageField(_("Avatar"), blank=True, null=True, upload_to=user_directory_path)

    #identificador unico
    uuid = models.UUIDField(default=uuid_lib.uuid4,
                            unique=True, null=False,
                            blank=False, editable=settings.DEBUG)


    def nombre_apellido(self):
        return self.primer_nombre + ' ' + self.primer_apellido

    def __str__(self):
        if self.tipo_documento and self.numero_documento:
            return "{} {}".format(self.tipo_documento, self.numero_documento)
        return self.nombre_apellido()

    def send_email(self, domain, protocol):
        self.email_confirm_key = generate_random_token(self.user.email)
        self.email_confirm_sent = timezone.now()

        context = {
            'email': self.user.email,
            'domain': domain,
            'site_name': settings.SITE_NAME,
            'user': self.user,
            'protocol': protocol,
            'key': self.email_confirm_key,
            'confirm_url': settings.CONFIRM_EMAIL_URL % self.email_confirm_key
        }
        from_email = None
        send_mail('accounts/register_user_subject.txt', "accounts/register_user_email.txt", context, from_email,
                  self.user.email, html_email_template_name="accounts/register_user_email.html")

    class Meta:
        verbose_name = _('Persona')
        verbose_name_plural = _('Personas')
        ordering = ('id', )


class DocumentoIdentificacion(BaseModel):
    tipo_documento = models.CharField(_("Tipo de documento"), null=True, blank=True,
                                        max_length=10, choices=DOCUMENT_CHOICES)    

    doc = models.FileField(_("Documento"), blank=True, null=True, upload_to=user_directory_path)

    class Meta:
        verbose_name = _('Documento de identificacion')
        verbose_name_plural = _('Documentos de identificacion')
        ordering = ('id', )


class AccesoCodigoEspecial(BaseModel):
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             null=True, blank=True)

    uuid = models.UUIDField(default=uuid_lib.uuid4,
                            unique=True, null=False,
                            blank=False, editable=settings.DEBUG)


    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)

    def __str__(self):
        return self.uuid

    def get_user(self):
        return self.persona.user

    class Meta:
        verbose_name = _('AccesoCodigoEspecial')
        verbose_name_plural = _('AccesoCodigoEspecial')
        ordering = ('id', )


class PersonaBaseModel(BaseModel):
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Direccion(BaseModel):
    linea_direccion = models.CharField(_("Address"), max_length=255, blank=True, null=True)
    calle = models.CharField(_("Street"), max_length=55, blank=True, null=True)
    ciudad = models.CharField(_("City"), max_length=255, blank=True, null=True)
    estado = models.CharField(_("State"), max_length=255, blank=True, null=True)
    codigo_postal = models.CharField(_("Post/Zip-code"), max_length=64, blank=True, null=True)

    def __str__(self):
        return self.city if self.city else ""

    class Meta:
        verbose_name = _('Direccion')
        verbose_name_plural = _('Direcciones')
        ordering = ('id', )


class TipoCorreo(BaseModel):
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                             null=True, blank=True)
    tipo_correo = models.CharField(max_length=50,unique=True)

    def __str__(self):
        return self.tipo_correo

    class Meta:
        verbose_name = _('Tipo de correo')
        verbose_name_plural = _('Tipos de correos')
        ordering = ('id', )


class Correo(PersonaBaseModel):
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT,
                             null=True, blank=True)
    tipo_correo = models.ForeignKey(TipoCorreo, on_delete=models.CASCADE)
    correo = models.EmailField()

    def __str__(self):
        return self.correo

    class Meta:
        verbose_name = _('Correp')
        verbose_name_plural = _('Correos')
        ordering = ('id', )
        unique_together = ("persona", "correo")


class TipoTelefono(BaseModel):
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT,
                             null=True, blank=True)
    tipo_telefono = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.tipo_telefono

    class Meta:
        verbose_name = _('Tipo de telefono')
        verbose_name_plural = _('Tipos de telefonos')
        ordering = ('id', )


class Telefono(PersonaBaseModel):
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                null=True, blank=True)
    tipo_telefono = models.ForeignKey(TipoTelefono, on_delete=models.CASCADE)
    telefono = models.CharField(max_length=15)
    principal = models.NullBooleanField(default=None)


    def __str__(self):
        return self.telefono

    class Meta:
        verbose_name = _('Telefono')
        verbose_name_plural = _('Telefonos')
        ordering = ('id', )
        unique_together = ("persona", "telefono")


RELATIONSHIP_CHOICES = (
    ("mother", _("Mother")),
    ("father", _("Father")),
    ("parent", _("Parent")),
    ("brother", _("Brother")),
    ("sister", _("Sister")),
    ("child", _("Child")),
    ("friend", _("Friend")),
    ("spouse", _("Spouse")),
    ("partner", _("Assistant")),
    ("manager", _("Manager")),
    ("other", _("Other")),
    ("doctor", _("Doctor")),
)


class ContactoEmergencia(PersonaBaseModel):
    """
        Informacion para contacto de emergencia
    """

    relacion = models.CharField(_("Relationship"), max_length=30, choices=RELATIONSHIP_CHOICES,
                                    blank=True)

    def __str__(self):
        return "{} {}".format(self.persona.primer_nombre,
                              self.persona.primer_apellido)

    class Meta:
        verbose_name = _('Contacto de emergencia')
        verbose_name_plural = _('Contactos de emergencia')
        ordering = ('id', )


class Hijo(PersonaBaseModel):

    hijo = models.ForeignKey(Persona, on_delete=models.CASCADE,
                             related_name="hijo_persona")

    def __str__(self):
        return "{} {}".format(self.persona.primer_nombre,
                              self.persona.primer_apellido)

    class Meta:
        verbose_name = _('Hijo')
        verbose_name_plural = _('Hijos')
        ordering = ('id', )


class Mascota(PersonaBaseModel):
    """
        Guarda la mascota de una persona.
    """
    nombre = models.CharField(_("Name"), max_length=30, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Mascota')
        verbose_name_plural = _('Mascotas')
        ordering = ('id', )
