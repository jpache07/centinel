import jwt
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.generics import RetrieveAPIView, CreateAPIView, UpdateAPIView, ListAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser,MultiPartParser
from common.mixins import BaseAPIView, BasePostAPIView, BulkAPIView, \
    MultipleChoicesAPIView, MultipleChoicesViewSet
from .permissions import SuperUserUOtro, MismaPersonaOSuperUserPermission, IsNotAuthenticated
from common.pagination import LargeResultsSetPagination
from .serializers import *
from .models import SEXO_CHOICES, TIPO_SANGRE_CHOICES, Persona,\
    DOCUMENT_CHOICES, Telefono, TipoTelefono, Correo, TipoCorreo
from rest_framework_jwt.utils import jwt_decode_handler
from rest_framework_jwt.views import JSONWebTokenAPIView
from rest_framework_jwt.settings import api_settings

jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER
User = get_user_model()


class UserView(RetrieveAPIView, CreateAPIView, BaseAPIView):
    """
    get:
        regresa el usuario de session

    post:
        crea un nuevo usuario en el sistema

    """
    serializer_class = CreateUserSerializer
    #permission_classes = (AllowAny,)

    def get_permissions(self):
        if self.request.method.lower() == "post":
            return [IsNotAuthenticated()]

        return super(UserView, self).get_permissions()

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return UserDetailSerializer
        return self.serializer_class

    def get_object(self):
        return self.request.user


class UserViewSet(RetrieveAPIView, UpdateAPIView, BaseAPIView):
    """
    get:
        Regresa la informacion de un usuario por su id.

    patch:
        Actualiza la informacion de un usuario por su id.

    """
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()
    lookup_field = 'pk'

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return UpdateUserSerializer
        return self.serializer_class

    def get_permissions(self):
        if self.request.method.lower() == "post":
            return [AllowAny()]

        #if self.action == 'retrieve':
        #    return [MismaPersonaOSuperUserPermission()]

        return super(UserViewSet, self).get_permissions()


class UserFormChoicesList(MultipleChoicesAPIView):
    """
    List all choices for users form fields. gender, blood_type, tip_type
    """
    choices_response = {"sexo": SEXO_CHOICES, "tipo_sangre": TIPO_SANGRE_CHOICES}


class PasswordResetSendEmail(BaseAPIView):
    """
    post:
    Recibe el correo del usuario y le envia la url 
    para cambiar la clave

    """
    serializer_class = PasswordResetSendEmailSerializer

    def get_permissions(self):
        return [AllowAny()]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response({"success": data})


class PasswordVerifyToken(BaseAPIView):
    """
    get:
    Check the token and uidb64. Returns success in case of success,
    otherwise the list of errors.

    """
    serializer_class = PasswordVerifyTokenSerializer

    def get_permissions(self):
        return [AllowAny()]

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=kwargs)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class PasswordReset(BaseAPIView):
    """
    post:
    Change the password of a user on the system, receive the uidb64,
    the new password and the confirmation of the new password. 
    Return success in case of change of key, otherwise a list of errors.

    """
    serializer_class = PasswordResetSerializer

    def get_permissions(self):
        return [AllowAny()]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})


class CambioClave(BaseAPIView):
    """
    post:
    Cambia la clave de un usuario logeado, recibe la clave actual
    la nueva clave y la verificacion de la clave.

    """
    serializer_class = CambioClaveSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"success": True})




# class AvatarViewSet(BasePersonaViewSet):
#     """
#     get:
#     Actualiza la foto del perfil persona de session

#     """
#     parser_classes = (MultiPartParser,FileUploadParser,)
#     serializer_class = AvatarCreateSerializer
#     permission_classes = (IsAuthenticated,)

#     def put(self, request, version, format=None):
        
#         serializer = self.get_serializer(data=request.data)
#         if serializer.is_valid(raise_exception=True):
#             serializer.save()
#             return Response(status=204)


class AvatarView( BaseAPIView):

    permission_classes = (AllowAny,)

    def get(self, request, version, format=None):
        
        try:
            with open(request.user.persona_set.avatar, "rb") as f:
                return HttpResponse(f.read(), content_type="image/jpeg")
        except IOError:
            red = Image.new('RGBA', (1, 1), (255,0,0,0))
            response = HttpResponse(content_type="image/jpeg")
            red.save(response, "JPEG")
            return response


class AvatarDetailView(BaseAPIView):

    permission_classes = (AllowAny,)

    def get(self, request, version, uuid, format=None):
        
        try:
            per = Persona.objects.get(uuid=uuid)
            return HttpResponse(per.avatar, content_type="image/jpeg")
            with open(per.avatar, "rb") as f:
                return HttpResponse(f.read(), content_type="image/jpeg")
        except IOError:
            red = Image.new('RGBA', (1, 1), (255,0,0,0))
            response = HttpResponse(content_type="image/jpeg")
            red.save(response, "JPEG")
            return response




class DocumentChoicesList(MultipleChoicesAPIView):
    """
    get:
        Lista de los tipos de documentos
    """
    choices_response = {"tipo": DOCUMENT_CHOICES}
    permission_classes = (AllowAny,)

     

class ValidacionesViewSet(BaseAPIView):
    """
    get:
        Valida el usuario, clave y correo

    """
    permission_classes = (AllowAny,)
    serializer_class = ValidacionesSerializer

    def get(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.GET)
        if serializer.is_valid(raise_exception=False):
            return Response({"success": True})
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)



class PersonaViewSet(viewsets.ModelViewSet, BaseAPIView):
    """
    get:
        Lista todas las personas del sistema.

    post:
        Crear una persona en el sistema.
    """
    serializer_class = PersonaSerializer
    queryset = Persona.objects.all()
    lookup_field = 'uuid'

    def get_serializer_class(self):

        if self.request.method == 'PATCH':
            return ActualizarPersonaSerializer
        return self.serializer_class

    def get_permissions(self):

        if self.action == 'create':
            return [AllowAny()]

        if self.action == 'retrieve':
            return [MismaPersonaOSuperUserPermission()]

        return [SuperUserUOtro()]
    
    
    # def post(self, request, *args, **kwargs):

    #     serializer = self.get_serializer(data=request.data)
    #     if serializer.is_valid(raise_exception=True):
    #         data = serializer.save()
    #         return Response({"success": True,'uuid':data.uuid},
    #                         status=status.HTTP_201_CREATED)




class BuscarPorDocumentoViewSet(BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = PersonaSerializer

    def get(self, request, *args, **kwargs):

        instance = get_object_or_404(Persona,
                   tipo_documento=kwargs.get('tipo'),
                   numero_documento=kwargs.get('documento'))
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class AsociarUsuarioPersonaViewSet(BasePostAPIView, BaseAPIView):
    """
    post:
        El usuario se asocia una persona, debe estar logueado
    """
    
    serializer_class = AsociarUsuarioPersonaSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)


class TelefonoPrincipalViewSet(UpdateAPIView, BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = TelefonosPersonaSerializer

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return TelefonoPrincipalPersonaSerializer
        return self.serializer_class

    def get(self, request, *args, **kwargs):

        persona = get_object_or_404(Persona,
                  uuid=kwargs.get('uuid'))
        try:
            instance = Telefono.objects.get(persona=persona,principal=True)
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        except Telefono.DoesNotExist:
            return Response({},status=404)


class TelefonosPersonaViewSet(CreateAPIView, BaseAPIView):

    serializer_class = TelefonosPersonaSerializer

    def get(self, request, *args, **kwargs):

        persona = get_object_or_404(Persona,
                  uuid=kwargs.get('uuid'))
        if persona.uuid != request.user.persona_set.uuid:
            raise PermissionDenied("Permiso denegado")
        instance = Telefono.objects.filter(persona=persona)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)        

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response(data, status=status.HTTP_201_CREATED)

    def get_permissions(self):

        if self.request.method.lower() == 'get':
            return [MismaPersonaOSuperUserPermission()]
        else:
            return [AllowAny()]



class MisTelefonosViewSet(CreateAPIView, BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = TelefonosPersonaSerializer

    def get(self, request, *args, **kwargs):

        persona = get_object_or_404(Persona,
                  uuid=kwargs.get('uuid'))
        instance = Telefono.objects.filter(persona=persona)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)        

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response(data, status=status.HTTP_201_CREATED)


class TelefonosPersonaBulkViewSet(BulkAPIView):

    permission_classes = (AllowAny,)
    serializer_class = TelefonosPersonaSerializer
    modelo = Telefono

    def modificar(self, object, **kwargs):
        instance = self.modelo.objects.get(id=object['id'])
        instance.principal = object['principal']
        instance.telefono = object['telefono']
        instance.save() 


class CorreosPersonaViewSet(CreateAPIView, BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = CorreosPersonaSerializer

    def get(self, request, *args, **kwargs):

        persona = get_object_or_404(Persona,
                  uuid=kwargs.get('uuid'))
        instance = Correo.objects.filter(persona=persona)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)        

    def post(self, request, *args, **kwargs):
        
        uuid = kwargs['uuid']
        persona = Persona.objects.get(uuid=uuid)
        data = {
            'tipo_correo':request.data['tipo_correo'],
            'correo':request.data['correo'],
            'persona':persona.id
        }
        serializer = self.get_serializer(data=data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response(data, status=status.HTTP_201_CREATED)


class CorreosPersonaBulkViewSet(BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = CorreosPersonaSerializer

    def crear(self, persona, object):

        if 'principal' in object:
            if object['principal'] == True:
                persona.user.email = object['correo']
                persona.user.save()
            else:
                try:
                    Correo.objects.get_or_create(
                    tipo_correo_id=object['tipo_correo_id'],
                    correo=object['correo'],
                    persona=persona)
                except Exception as e:
                    pass
            #otro error como que viene id y ya se elimino
        
    def post(self, request, *args, **kwargs):
        import json
        data = json.loads(request.body)
        persona = get_object_or_404(Persona,
                  uuid=kwargs.get('uuid'))

        for object in data:
            try:
                #print (object)
                if object['id'] != 'id_p_0':
                    if not 'modificar' in object:
                        pass
                        #Correo.objects.get(id=object['id']).delete()
                    else:
                        raise Exception
                else:
                    if object['principal'] == False:
                        Correo.objects.get_or_create(
                        tipo_correo_id=1,
                        correo=object['correo'],
                        persona=persona)
            except Exception as e:
                self.crear(persona, object)

        try:
            Correo.objects.get(correo=persona.user.email).delete()
        except Exception as e:
            pass

        resp = {"success": True}
        return Response(resp, status=status.HTTP_201_CREATED)


class TipoTelefonosViewSet(viewsets.ModelViewSet):

    permission_classes = (AllowAny,)
    serializer_class = TipoTelefonosSerializer

    def get_queryset(self):
        return TipoTelefono.objects.filter(condicion=True)


class TipoCorreosViewSet(viewsets.ModelViewSet):

    permission_classes = (AllowAny,)
    serializer_class = TipoCorreosSerializer

    def get_queryset(self):
        return TipoCorreo.objects.filter(condicion=True)



class PersonaPorJwtViewSet(BaseAPIView):

    permission_classes = (AllowAny,)
    serializer_class = PersonaPorJwtSerializer

    def get(self, request, *args, **kwargs):

        instance = get_object_or_404(Persona,
                   user=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class ObtainJSONWebToken(JSONWebTokenAPIView):
    """
    post:
        API View that receives a POST with a user's username and password.
        Returns a JSON Web Token that can be used for authenticated requests.
    """
    serializer_class = JSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():

            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            uuid = serializer.object.get('uuid')
            exp = serializer.object.get('exp')
            persona = serializer.object.get('persona')

            try:
                nacimiento = persona.nacimiento.strftime("%d/%m/%Y")
            except Exception as e:
                nacimiento = None

            respuesta = {
                'id': persona.id,            
                'token':token,
                'email': user.email,
                'usuario_id':user.id,
                'uuid': persona.uuid,
                'primer_nombre': persona.primer_nombre,
                'segundo_nombre': persona.segundo_nombre,
                'primer_apellido': persona.primer_apellido,
                'segundo_apellido': persona.segundo_apellido,
                'nacimiento': nacimiento,
                'sexo': persona.sexo,
                'tipo_documento': persona.tipo_documento,
                'numero_documento': persona.numero_documento,
                'nacionalidad_id': persona.nacionalidad.id if persona.nacionalidad else None,
                'nacionalidad': persona.nacionalidad.nacionalidad if persona.nacionalidad else None,
                'direccion': persona.direccion,
                'exp':exp,
                'username':user.username,
            }

            ### posible tarea asincrona
            # e_token = request.data.get('envio_token',None)
            # if e_token:
            #     from common.utils import envio_token
            #     from django.conf import settings
            #     import json
            #     resp = envio_token(e_token,{
            #         'uuid':persona.uuid,
            #         'system_uuid':settings.SYSTEM_UUID,
            #         'token':token,
            #         'exp':exp,
            #         'user_id': user.id,
            #         'grupos':json.dumps(grupos)
            #     })


            return Response(respuesta)


        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)