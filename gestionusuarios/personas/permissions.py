from rest_framework import permissions
from rest_framework_jwt.utils import jwt_decode_handler


class SuperUserUOtro(permissions.BasePermission):
    """
    Global permission check for Business User.
    """

    def has_permission(self, request, view):

        if request.user and request.user.is_staff:
            return True


class IsBusinessPermission(permissions.BasePermission):
    """
    Global permission check for Business User.
    """

    def has_permission(self, request, view):
        if request.method == "GET":
            return True

        return request.user.is_authenticated() and request.user.Persona.hasValidBusiness()


class IsCreatedPermission(permissions.BasePermission):
    """
    Global permission check object created
    """

    def has_object_permission(self, request, view, obj):
        if request.method == "GET":
            return True
        return request.user == obj.created_by


class OwnerOSuperUserPermission(permissions.BasePermission):
    """
    Si es el dueño o super admin.
    """

    def has_object_permission(self, request, view, obj):

        return (request.user == obj.created_by) or request.user.is_staff


class MismaPersonaOSuperUserPermission(permissions.BasePermission):
    """
    Si es la misma persona o super admin.
    """

    def has_object_permission(self, request, view, obj):
        
        if not request.user.id:
            return False
        return (obj.user == request.user) or request.user.is_staff

    def has_permission(self, request, view):
        #print (view.kwargs)
        if not request.user.id:
            return False        
        return True


class IsNotAuthenticated(permissions.BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        try:
            request.user.is_authenticated
            return False
        except Exception as e:
            print (e)
            return True



class AplicacionPermission(permissions.BasePermission):
    """
    Verifica que el token que recibe sea valido
    y los permisos que tiene
    """

    def has_object_permission(self, request, view, obj):
        auth = request.META.get('HTTP_AUTHORIZATION',None)[7:]
        
        return True

    def has_permission(self, request, view):      
        return True            