from datetime import timedelta, date

import pytest
from mixer.backend.django import mixer
from django.urls import reverse
from django.utils import timezone
from django.conf import settings
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator

from rest_framework import status
from rest_framework.test import APIClient

from conftest import VALID_MOBILE_NUMBER2, VALID_MOBILE_NUMBER
from tracking import models as models_tracking


@pytest.fixture
def visitor(user):
    visitor = mixer.blend(models_tracking.Visitor,
                          session_key="visitor", ip_address='192.1.1.1',
                          user=user, user_agent='fake', referrer='referrer',
                          url='url', page_views=1, session_start=timezone.now(),
                          last_update=timezone.now())


@pytest.fixture
def user():
    user = mixer.blend(settings.AUTH_USER_MODEL, username="test", email="jovanjpacheco@gmail.com")
    user.set_password("testing123")
    user.save()
    user.profile.mobile = VALID_MOBILE_NUMBER
    user.profile.save()
    return user


@pytest.fixture
def generate_token(user):
    uid = urlsafe_base64_encode(force_bytes(user.pk)).decode()
    token = default_token_generator.make_token(user)
    return uid, token


@pytest.mark.django_db
class TestAccount:
    def test_login(self):
        factory = APIClient()


@pytest.mark.django_db
class TestUserViewSet:
    def test_create_user(self):
        factory = APIClient()

        request = factory.post(reverse("api:user", args=("v1",)), {
            "birth_date": "1990-07-19",
            "email": "test@example.com",
            "first_name": "Test",
            "last_name": "Lastname",
            "mobile": VALID_MOBILE_NUMBER,
            "password": "testing123"
        })
        assert request.status_code == status.HTTP_201_CREATED

    def test_edit_user(self, user, api_factory_authenticate):
        request = api_factory_authenticate.patch(reverse("api:user", args=("v1",)), {
            "birth_date": "1990-07-20",
        })

        assert request.status_code == status.HTTP_200_OK
        user.refresh_from_db()
        assert user.profile.birth_date == date(year=1990, month=7, day=20)


@pytest.mark.django_db(transaction=True)
class TestMobileView:
    def test_not_allowed_method(self, api_factory_authenticate):
        request = api_factory_authenticate.patch(
            reverse("api:user-mobile", args=("v1",)))
        assert request.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    def test_code_validation(self, user, api_factory_authenticate):
        user.profile.generate_mobile_verification_number()
        user.profile.save()
        request = api_factory_authenticate.post(reverse("api:user-mobile", args=("v1",)),
                                                {"code": user.profile.mobile_verification_number})
        assert request.status_code == status.HTTP_200_OK

    def test_code_validation_invalid(self, user, visitor, api_factory_authenticate):
        request = api_factory_authenticate.post(
            reverse("api:user-mobile", args=("v1",)), {"code": "1111111"})

        assert request.status_code == status.HTTP_400_BAD_REQUEST

    def test_resend_code_valid(self, user, visitor, api_factory_authenticate):
        user.profile.mobile_verification_time = timezone.now() - timedelta(minutes=31)

        user.profile.save()

        request = api_factory_authenticate.get(
            reverse("api:user-mobile", args=("v1",)))
        assert request.status_code == status.HTTP_200_OK
        assert request.json()["success"] is True


    def test_resend_code_invalid(self, user, api_factory_authenticate):
        user.profile.generate_mobile_verification_number()
        user.profile.save()

        request = api_factory_authenticate.get(
            reverse("api:user-mobile", args=("v1",)))
        assert request.status_code == status.HTTP_403_FORBIDDEN


    def test_change_mobile_valid(self, user, visitor, api_factory_authenticate):
        user.profile.mobile_verification_time = timezone.now() - timedelta(minutes=31)
        user.profile.save()

        request = api_factory_authenticate.put(
            reverse("api:user-mobile", args=("v1",)), {"mobile": VALID_MOBILE_NUMBER2})
        assert request.status_code == status.HTTP_200_OK


    def test_change_mobile_invalid_number(self, user, api_factory_authenticate):
        user.profile.mobile_verification_time = timezone.now() - timedelta(minutes=31)
        user.profile.save()

        request = api_factory_authenticate.put(
            reverse("api:user-mobile", args=("v1",)), {"mobile": VALID_MOBILE_NUMBER})
        assert request.status_code == status.HTTP_400_BAD_REQUEST, "The number its repeated. because already exists a" \
                                                                   " user with this number "

        request = api_factory_authenticate.put(
            reverse("api:user-mobile", args=("v1",)), {"mobile": "111"})
        assert request.status_code == status.HTTP_400_BAD_REQUEST, "The number its invalid"


    def test_change_mobile_invalid_resend_time_short(self, user, api_factory_authenticate):
        user.profile.mobile_verification_time = timezone.now()
        user.profile.save()

        request = api_factory_authenticate.put(
            reverse("api:user-mobile", args=("v1",)), {"mobile": VALID_MOBILE_NUMBER})
        assert request.status_code == status.HTTP_400_BAD_REQUEST


    def test_password_send_email(self, user, api_factory_authenticate):
        request = api_factory_authenticate.post(
            reverse("api:password_reset_send_email", args=("v1",)), {"email": user.email})
        assert request.status_code == status.HTTP_200_OK


    def test_password_verify_token(self, user, api_factory_authenticate, generate_token):
        uid, token = generate_token
        k = {'uidb64': uid, 'token': token, 'version': 'v1'}

        request = api_factory_authenticate.get(
            reverse("api:password_reset_verify", kwargs=k))
        print(request.content)
        assert request.status_code == status.HTTP_200_OK


    def test_password_reset(self, user, api_factory_authenticate, generate_token):
        uid, token = generate_token
        data = {
            "uidb64": uid,
            "new_password1": "123testpassword",
            "new_password2": "123testpassword"
        }

        request = api_factory_authenticate.post(
            reverse("api:password_reset", args=("v1",)),
            data)

        assert request.status_code == status.HTTP_200_OK
