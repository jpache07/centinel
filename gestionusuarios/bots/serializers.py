from importlib import import_module
from django.conf import settings
from django.core.exceptions import ValidationError as DjangoValidationError
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import NotFound
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import HyperlinkedModelSerializer, raise_errors_on_nested_writes

from common.serializers import QueryFieldsMixin
from personas.models import Persona 
from .models import Bot, BotPersona, PermisoBot


class BotSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    #persona =  serializers.HyperlinkedRelatedField(view_name="personas:persona-detail") 
    #persona = serializers.PrimaryKeyRelatedField(many=False, queryset=Persona.objects.all())
    #user = serializers.PrimaryKeyRelatedField(many=False, queryset=get_user_model().objects.all())

    class Meta:
        model = Bot
        exclude = ('created','modified','condicion')



class BotEnviarMensajeSerializer(serializers.Serializer):

    mensaje = serializers.CharField(required=True)

    def validate(self, attrs):
        attrs['user'] = self.context['request'].user
        attrs['uuid'] = self.context['view'].kwargs['uuid']
        return attrs

    def create(self, validated_data):

        data = {}
        try:

            bot = Bot.objects.get(persona__uuid=validated_data['uuid'])
            if bot.tipo == 3:

                desarrollos = import_module("desarrollos.views")
                funcion = getattr(desarrollos, bot.endpoint)
                persona = Persona.objects.get(user=validated_data['user'])

                parametros_bot = {
                    'peticion': {
                        'tipo':'POST'
                    },
                    'persona': {
                        'primer_nombre':persona.primer_nombre,
                        'primer_apellido':persona.primer_apellido,
                        'uuid':persona.uuid
                    },
                    'mensaje': validated_data.pop('mensaje')
                }

                respuesta = funcion(**parametros_bot)
                data = {'mensaje': str(respuesta)}

        except Bot.DoesNotExist:
            data = {'error': 'El bot proporcionado no existe.'}
        
        except AttributeError as er:

            data = {'error': 'El endpoint esta mal configurado'}

        except Exception as e:
            print (e)
            print (type(e))
            data = {'error':str(e)}

        finally:
            return data



class BotInstalarSerializer(serializers.Serializer):

    bot = serializers.UUIDField(required=True)
    persona = serializers.UUIDField(required=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    #permisos 

    def validate(self, attrs):
        #attrs['user'] = self.context['request'].user
        return attrs

    def create(self, validated_data):

        data = {}
        try:
            u_bot  = validated_data.get('bot')
            u_per  = validated_data.get('persona')

            bot = Bot.objects.get(persona__uuid=u_bot)
            persona = Persona.objects.get(uuid=u_per)

            #BotPersona.objects.get_or_create()
            data = {'mensaje': 'Bot instalado con exito'}

        except Bot.DoesNotExist:
            data = {'error': 'El bot proporcionado no existe.'}
        
        except AttributeError as er:

            data = {'error': 'El endpoint esta mal configurado'}

        except Exception as e:
            print (e)
            print (type(e))
            data = {'error':str(e)}

        finally:
            return data


class PermisoBotSerializer(QueryFieldsMixin,
                           HyperlinkedModelSerializer):

    
    #url = serializers.HyperlinkedIdentityField(view_name="myapp:user-detail")
    
    class Meta:
        model = PermisoBot
        exclude = ('created','modified','condicion')