import uuid as uuid_lib
from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _

from personas.models import Persona
from common.models import UserRelation, BaseModel

class PermisoBot(BaseModel):

    nombre = models.CharField(_('nombre'), max_length=255)
    codigo = models.CharField(_('codigo'), max_length=100)

    def __str__(self):
        str_r = "{} - {}".format(self.nombre, self.codigo)
        return str_r

    class Meta:
        verbose_name = _('Permiso para Bot')
        verbose_name_plural = _('Permisos para Bot')
        ordering = ('id', )


TIPOS_BOT = (
    (1, 'url'),
    (2, 'pc'),
    (3, 'nativo'),
)

class Bot(UserRelation):

    #user aqui es el creador del bot
    nombre = models.CharField(_("nombre"), max_length=65)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    tipo = models.SmallIntegerField(_("tipo"), choices=TIPOS_BOT)
    endpoint = models.CharField(_("endpoint"), max_length=965)
    permisos = models.ManyToManyField(
        PermisoBot,
        verbose_name=_('permisos'),
        blank=True,
    )
    bot_grupal = models.NullBooleanField(_("bot_grupal"), default=None)


    def __str__(self):
        str_r = "{}".format(str(self.nombre))
        return str_r

    class Meta:
        verbose_name = _('Bot')
        verbose_name_plural = _('Bots')
        ordering = ('id', )



class BotPersona(UserRelation):

    #user es quien le instalo el bot a la persona (el mismo u otro)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    bot = models.ForeignKey(Bot, on_delete=models.CASCADE)
    permisos = models.ManyToManyField(
        PermisoBot,
        verbose_name=_('permisos'),
        blank=True,
    )

    def __str__(self):
        str_r = "{} {}".format(str(self.persona),str(self.bot))
        return str_r

    class Meta:
        verbose_name = _('Bot persona')
        verbose_name_plural = _('Bots para persona')
        ordering = ('id', )


class ChatBot(UserRelation):
    # user es quien crea el chat para el bot (el mismo u otro)
    uuid = models.UUIDField(default=uuid_lib.uuid4,
                            unique=True, null=False,
                            blank=False, editable=settings.DEBUG)

    persona = models.ManyToManyField(
        Persona,
        verbose_name=_('Personas'),
        blank=True,
    )

    bot = models.ManyToManyField(
        Bot,
        verbose_name=_('Bot'),
        blank=True,
    )

    def __str__(self):
        str_r = "{}".format(str(self.uuid))
        return str_r

    class Meta:
        verbose_name = _('Chat bot')
        verbose_name_plural = _('Chats bot')
        ordering = ('id', )


MESSAGE_TYPES = (
    ('TEXT', 'Simple test'),
    ('TEXT_EMOJI', 'Text with emoji'),
    ('MENU_ITEM', 'Menu item shared'),
    ('PAYMENT_CONTEXTUAL', 'Payment Request Contextual'),
    ('PAYMENT_MANUAL', 'Payment Request manual entry'),
    ('DOCUMENT_REQUEST', 'Document Request'),
    ('DOCUMENT_SUBMISSION', 'Document Submission'),
    ('SIGNATURE_REQUEST', 'Digital Signature Request'),
    ('SIGNATURE_COMPLETED', 'Digital Signature Completed'),
)


class MensajeContexto(BaseModel):
    type_message = models.CharField(max_length=25,
                                    choices=MESSAGE_TYPES,
                                    default="TEXT")
    context = JSONField()


    def __str__(self):
        str_r = "{type}"
        return str_r.format(
            type=self.type_message)


    class Meta:
        verbose_name = _('Mensaje Contexto')
        verbose_name_plural = _('Mensaje contextos')
        ordering = ('id', )


class MensajeBot(UserRelation):

    de =   models.ForeignKey(Persona, on_delete=models.CASCADE)
    para = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    message_context = models.ForeignKey(MensajeContexto,
                                        null=True, blank=True,
                                        on_delete=models.PROTECT)
    message_body = models.TextField()

    def __str__(self):
        str_r = "{user} dijo \"{message}\" ({created})"
        return str_r.format(
            user=self.de,
            message=self.message_body,
            created=self.created)

    def get_context(self):
        return self.message_context.context

    class Meta:
        verbose_name = _('Mensaje Bot')
        verbose_name_plural = _('Mensajes bot')
        ordering = ('id', )

