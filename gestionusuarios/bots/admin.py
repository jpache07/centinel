from django.contrib import admin
from common.admin import BaseAdmin
from .models import PermisoBot, Bot, BotPersona, ChatBot, MensajeBot, MensajeContexto


@admin.register(PermisoBot)
class PermisoBotAdmin(BaseAdmin):
    pass


@admin.register(Bot)
class BotAdmin(BaseAdmin):
    pass


@admin.register(BotPersona)
class BotPersonaAdmin(BaseAdmin):
    pass


@admin.register(ChatBot)
class ChatBotAdmin(BaseAdmin):
    pass


@admin.register(MensajeBot)
class MensajeBotAdmin(BaseAdmin):
    pass


@admin.register(MensajeContexto)
class MensajeContextoAdmin(BaseAdmin):
    pass




