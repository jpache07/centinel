from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()
router.register("bots", BotViewSet,'bot')
router.register("bots/permisos", PermisosBotViewSet,'permisos_bot')

urlpatterns = [

    url(r'^bots/enviar_mensaje/(?P<uuid>[-\w]+)/', BotView.as_view(), name="llamar_bot"),
    url(r'^bots/instalar/', BotView.as_view(instalar=True), name="instalar_bot"),
    url(r'^', include(router.urls)),

]
