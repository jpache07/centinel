from rest_framework.generics import RetrieveAPIView, CreateAPIView
from rest_framework.views import Response
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny

from common.mixins import BaseAPIView
from .models import Bot, PermisoBot
from .serializers import BotEnviarMensajeSerializer, BotInstalarSerializer,\
    PermisoBotSerializer, BotSerializer


class BotView(BaseAPIView):
    """
    post:
        Envia un mensaje al bot por uuid

    """
    instalar = False
    serializer_class = BotEnviarMensajeSerializer

    def get_serializer_class(self):
        if self.instalar:
            return BotInstalarSerializer
        return self.serializer_class


    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response(data)        


class BotViewSet(viewsets.ModelViewSet):

    serializer_class = BotSerializer
    lookup_field = 'pk'

    def get_queryset(self):
        return Bot.objects.all()


    def get_permissions(self):
        method = self.request.method.lower()

        if method == "get":
            return [AllowAny()]

        return super(BotViewSet, self).get_permissions()


class PermisosBotViewSet(viewsets.ModelViewSet):

    serializer_class = PermisoBotSerializer
    lookup_field = 'pk'

    def get_queryset(self):
        print (str(self.__class__))
        return PermisoBot.objects.all()