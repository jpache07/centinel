import uuid
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import models
from common.models import UserOneBaseModel, BaseModel
from personas.models import PersonaBaseModel


class Solicitud(PersonaBaseModel):

    """ Solicitudes de trasferencias o paginas de patria"""

    titulo = models.CharField(_('titulo'), max_length=255)
    uuid = models.UUIDField(default=uuid.uuid4,
                            unique=True, editable=settings.DEBUG)
    descripcion = models.TextField()

    def __str__(self):
        r = "{} - {}".format(self.titulo, self.persona)
        return r

    class Meta:
        verbose_name = _('Solicitud')
        verbose_name_plural = _('Solicitudes')
        ordering = ('id', )