import uuid
from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver

from personas.models import Persona
from common.models import UserRelation, BaseModel, UserRelationNull


class Banco(BaseModel):

    nombre = models.CharField(_('nombre'), max_length=255)
    codigo = models.CharField(_('codigo'), max_length=100)

    def __str__(self):
        str_r = "{} - {}".format(self.nombre, self.codigo)
        return str_r

    class Meta:
        verbose_name = _('Banco')
        verbose_name_plural = _('Bancos')
        ordering = ('id', )



class TipoMoneda(BaseModel):

    nombre = models.CharField(_('nombre'), max_length=255)
    simbolo = models.CharField(_('simbolo'), max_length=3)

    def __str__(self):
        str_r = "{} - {}".format(self.nombre, self.simbolo)
        return str_r

    class Meta:
        verbose_name = _('Tipo de moneda')
        verbose_name_plural = _('Tipos de moneda')
        ordering = ('id', )



class Moneda(UserRelation):

    cantidad = models.DecimalField(default=0, max_digits=30, decimal_places=2)
    uuid = models.UUIDField(default=uuid.uuid4,
                            unique=True, null=False,
                            blank=False, editable=settings.DEBUG)
    tipo = models.ForeignKey(TipoMoneda, on_delete=models.CASCADE)

    def __str__(self):
        str_r = "{} - {}".format(self.nombre, self.simbolo)
        return str_r

    class Meta:
        verbose_name = _('Tipo de moneda')
        verbose_name_plural = _('Tipos de moneda')
        ordering = ('id', )


class Billetera(UserRelationNull):
    total = models.DecimalField(default=0, max_digits=30, decimal_places=2)
    total_disponible = models.DecimalField(default=0, max_digits=30, decimal_places=2)
    tipo = models.ForeignKey(TipoMoneda, on_delete=models.CASCADE)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)

    def __str__(self):
        str_r = "{} - {}".format(self.persona, self.total_disponible)
        return str_r


@receiver(post_save, sender=Persona)
def save_user_wallet(sender, instance, **kwargs):

    t,c = TipoMoneda.objects.get_or_create(nombre='bolivar',
    									   simbolo='Bs.')	
    try:
        Billetera.objects.get(persona=instance,
        					  tipo=t)
    except Billetera.DoesNotExist:
        Billetera.objects.create(persona=instance,
        						 tipo=t
        						 )