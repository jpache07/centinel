from rest_framework.generics import RetrieveAPIView, CreateAPIView
from rest_framework.views import Response
from rest_framework import viewsets
from common.mixins import BaseAPIView
from .models import App, PermisoApp
from .serializers import AppInstalarSerializer,\
    PermisoAppSerializer, AppSerializer


class App12View(BaseAPIView):
    """
    post:
        Instalar app

    """
    serializer_class = AppInstalarSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            data = serializer.save()
            return Response(data)        


class PermisosAppViewSet(viewsets.ModelViewSet):

    serializer_class = PermisoAppSerializer
    lookup_field = 'pk'

    def get_queryset(self):

        return PermisoApp.objects.all()


class AppViewSet(viewsets.ModelViewSet):

    serializer_class = AppSerializer
    lookup_field = 'pk'

    def get_queryset(self):

        return App.objects.all()


        