from django.contrib import admin
from common.admin import BaseAdmin
from .models import Banco,Billetera


@admin.register(Banco)
class BancoAdmin(BaseAdmin):
    pass


@admin.register(Billetera)
class BilleteraAdmin(BaseAdmin):
    pass


