from importlib import import_module
from django.conf import settings
from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework import serializers
from rest_framework.exceptions import NotFound
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import HyperlinkedModelSerializer, raise_errors_on_nested_writes

from common.serializers import QueryFieldsMixin
from personas.models import Persona 
from .models import PermisoApp, App



class AppSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = App
        exclude = ('created','modified','condicion')


class AppInstalarSerializer(serializers.Serializer):

    app = serializers.UUIDField(required=True)
    persona = serializers.UUIDField(required=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    #permisos 

    def validate(self, attrs):
        #attrs['user'] = self.context['request'].user
        return attrs

    def create(self, validated_data):

        data = {}
        try:
            u_app  = validated_data.get('app')
            u_per  = validated_data.get('persona')

            app = App.objects.get(persona__uuid=u_app)
            persona = Persona.objects.get(uuid=u_per)

            #AppPersona.objects.get_or_create()
            data = {'mensaje': 'App instalada con exito'}

        except App.DoesNotExist:
            data = {'error': 'La app proporcionado no existe.'}
        
        except AttributeError as er:

            data = {'error': 'El endpoint esta mal configurado'}

        except Exception as e:
            print (e)
            print (type(e))
            data = {'error':str(e)}

        finally:
            return data


class PermisoAppSerializer(QueryFieldsMixin,
                           HyperlinkedModelSerializer):

    
    url = serializers.HyperlinkedIdentityField(view_name="myapp:user-detail")
    
    class Meta:
        model = PermisoApp
        exclude = ('created','modified','condicion')