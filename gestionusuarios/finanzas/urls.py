from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers
from rest_framework.routers import SimpleRouter
#from .views import *

prefix = "bancos"
router = SimpleRouter()
#router.register("%s" % prefix, AppViewSet,'apps')
#router.register("%s/permisos" % prefix, PermisosAppViewSet,'permisos_app')

urlpatterns = [

    url(r'^', include(router.urls)),

]
