# -*- coding: utf-8 -*-
from .base import *

DEBUG = True

ALLOWED_HOSTS = ['localhost']

EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'

STATIC_URL = 'https://staticsisttoo.herokuapp.com/'

STATIC_ROOT = join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    join(BASE_DIR, 'staticfiles'),
)

