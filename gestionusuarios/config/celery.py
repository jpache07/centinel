from celery import Celery
import os
import redis
from celery import shared_task

app = Celery('config')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local_development')

app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
# app.conf.update(
#     BROKER_URL = 'redis://192.168.99.100:6379/0',
#     CELERY_RESULT_BACKEND = 'redis://192.168.99.100:6379/0',
# )
#app.conf.broker_url = 'redis://192.168.99.100:6379/0'

@app.task(bind=True)
def debug_task(self):
	print('Request: {0!r}'.format(self.request))

