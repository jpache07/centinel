from django.conf import settings
from django.urls import path
from django.conf.urls import include, handler404, handler500, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf.urls.static import static
from django.views.static import serve
from django.views.generic.base import RedirectView
from common.views import HomeView, View404, View500
from common.views import SwaggerSchemaView

from apps.views import AppView

urlpatterns_templates = [
    path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico')),
 
    url(r'^apps/instalar/(?P<token>[-\w]+)/$', AppView.as_view(), name="app_instalar"),
    path('', HomeView.as_view(), name="inicio"),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #path('<version>/', include('personas.urls', namespace='personas')),
    path('<version>/', include('personas.urls'), name='personas'),
    path('<version>/', include('bots.urls'), name="bots"),
    path('<version>/', include('apps.urls'), name="apps"),
    path('<version>/', include('common.urls'), name="common"),
    path('api/docs', SwaggerSchemaView.as_view()),
    path('', include(urlpatterns_templates)),   


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


handler404 = View404.as_view()
handler500 = View500.as_view()


if settings.DEBUG:


    import sys
    if "debug_toolbar" in sys.modules:
        import debug_toolbar

        urlpatterns = [
                          path('__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns