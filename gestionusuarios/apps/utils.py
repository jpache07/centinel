from calendar import timegm
from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework_jwt.settings import api_settings

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

def crear_token(app, per):

    payload = {
        'app': str(app),
        'per': str(per),
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }

    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )


    t = jwt_encode_handler(payload)
    return t


def decode_token(token):
	data = jwt_decode_handler(token)
	return data