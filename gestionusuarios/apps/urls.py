from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt
from rest_framework import routers
from rest_framework.routers import SimpleRouter
from .views import *

prefix = "apps"
router = SimpleRouter()
router.register("%s" % prefix, AppViewSet,'apps')
router.register("%s/permisos" % prefix, PermisosAppViewSet,'permisos_app')


urlpatterns = [
    url(r'^%s/instalacion/' % prefix , InstalacionAppView.as_view(), name="instalar_app"),
    url(r'^%s/get_token/tai/(?P<tai>[-\w]+)/$' % prefix ,AppGetToken.as_view(), name="app_get_token"),    
    url(r'^%s/jugador_js/' % prefix , JugadorJavascript.as_view(), name="jugador_js"),
    url(r'^aplications/(?P<app>[-\w]+)/(?P<per>[-\w]+)/', AppView.as_view(), name="app_get"),
    url(r'^', include(router.urls)),
]
