import time
import hashlib
from rest_framework_jwt.settings import api_settings
from jwt.exceptions import ExpiredSignatureError

from rest_framework.generics import RetrieveAPIView, CreateAPIView, get_object_or_404
from rest_framework.views import Response
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from django.http import JsonResponse
from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth import get_user_model
from django.views.generic.base import TemplateResponseMixin, TemplateView
from common.mixins import BaseAPIView

from personas.models import Persona
from personas.permissions import AplicacionPermission
from .models import App, PermisoApp, AppPersona, TokenAppInstalar
from .serializers import AppInstalarSerializer,\
    PermisoAppSerializer, AppSerializer, AppTokenSerializer,\
    JugadorJavascriptSerializer
from .utils import decode_token



class InstalacionAppView(CreateAPIView, BaseAPIView):
    """
    POST:
        Aplicacion instalada, crea token y redirecciona a app

    """
    permission_classes = [AllowAny,]
    serializer_class = AppInstalarSerializer
    queryset = AppPersona.objects.all()


class AppGetToken(RetrieveAPIView, BaseAPIView):

    """
    GET: 
        Devuelve el token si el usuario tiene
        la aplicacion instalada
    """
    serializer_class = AppTokenSerializer
    permission_classes = [AllowAny,]
    
    def get_object(self):

        tai = self.kwargs['tai']
        tai = get_object_or_404(TokenAppInstalar, token=tai)
        
        filter_kwargs = {
            'persona':tai.persona,
            'app__persona':tai.app.persona
        }

        obj = get_object_or_404(AppPersona, **filter_kwargs)
        tai.delete()
        return obj



class AppView(TemplateResponseMixin, BaseAPIView):
    """
    POST:
        Solicitar instalacion o jugar.

    GET:
        Peticion de permiso para instalar.

    """
    permission_classes = [AllowAny,]
    template_name = 'autorizacion_simple.html'


    def get_permissions(self):
        if self.request.method.lower() == "get":
            return [AplicacionPermission()]

        return super(AppView, self).get_permissions()

    def get(self, request, token, *args, **kwargs):

        try:
            t = TokenAppInstalar.objects.get(token=token)
            data = t
            t.delete()
            token_app = bytes(str(time.time()), 'utf8')
            secret = hashlib.sha224(token_app).hexdigest()
            tai = TokenAppInstalar.objects.create(app=data.app,
                                                  token=secret,
                                                  persona=data.persona)

            nt = hashlib.sha224(bytes(str(tai.token), 'utf8')).hexdigest()

            tai_instalar =  TokenAppInstalar.objects.create(app=data.app,
                            token=nt,
                            persona=data.persona)

            return self.render_to_response({
                'tai':tai.token,
                'app':tai.app,
                'persona':data.persona,
                'tai_instalar':tai_instalar.token
            })

        except Exception as e:
            print (e)
            self.template_name = 'autorizacion_token_invalido.html'
            return self.render_to_response({})


    def post(self, request, app, per, *args, **kwargs):
        
        jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
        token = request.POST.get('ut')
        token_app = bytes(token + str(time.time()), 'utf8')
        secret = hashlib.sha224(token_app).hexdigest()

        try:
            
            decode = jwt_decode_handler(token)
        except ExpiredSignatureError as ex_s:
            return redirect('http://192.168.1.213:9000/login/')

        user = get_user_model().objects.get( username=decode['username'])
        persona = Persona.objects.get(uuid=per, user=user)

        app = App.objects.get(persona__uuid=app)


        if app.instalable:
            h = "%s://%s" % ('https' if request.is_secure() else 'http', request.get_host())
            try:
                ap = AppPersona.objects.get(app=app,
                                            persona=persona,
                                            condicion=True)
                

                tai = TokenAppInstalar.objects.create(app=app,token=secret,persona=persona)
                self.template_name = 'autorizacion_redirect_app.html'
                return self.render_to_response({
                    'tai':tai.token
                })
            except AppPersona.DoesNotExist:
                
                TokenAppInstalar.objects.create(app=app,token=secret,persona=persona)
                r = h + '/apps/instalar/%s/' % secret
        else:
            r = app.redirect_url
        return redirect(r)


class PermisosAppViewSet(viewsets.ModelViewSet):

    serializer_class = PermisoAppSerializer
    lookup_field = 'pk'

    def get_queryset(self):

        return PermisoApp.objects.all()


class AppViewSet(viewsets.ModelViewSet):

    serializer_class = AppSerializer
    lookup_field = 'pk'

    def get_permissions(self):
        if self.request.method.lower() == "get":
            return [AllowAny()]

        return super(AppViewSet, self).get_permissions()

    def get_queryset(self):
        return App.objects.all()


        
class JugadorJavascript(RetrieveAPIView, BaseAPIView):
    """
    Devuelve datos iniciales para un jugador
    """
    permission_classes = [AplicacionPermission,]
    serializer_class = JugadorJavascriptSerializer
    
    def get_serializer_context(self):
        return {
            'request': self.request,
            'view': self,
            'app_persona':self.app_persona
        }

    def get_object(self):

        auth = self.request.META.get('HTTP_AUTHORIZATION',None)[7:]
        data = decode_token(auth)
        aplicacion = App.objects.get(persona__uuid=data['app'])
        obj = Persona.objects.get(uuid=data['per'])
        self.app_persona = AppPersona.objects.get(app=aplicacion, persona=obj)
        self.check_object_permissions(self.request, obj)
        return obj