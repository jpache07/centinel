from django.contrib import admin
from common.admin import BaseAdmin
from .models import PermisoApp, App, AppPersona, TokenAppInstalar



@admin.register(PermisoApp)
class PermisoAppAdmin(BaseAdmin):
    pass

@admin.register(App)
class AppAdmin(BaseAdmin):
    pass

@admin.register(AppPersona)
class AppPersonaAdmin(BaseAdmin):
    pass

@admin.register(TokenAppInstalar)
class TokenAppInstalarAdmin(BaseAdmin):
    list_display = ('token','app')
