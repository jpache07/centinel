from importlib import import_module
from django.conf import settings
from django.core.exceptions import ValidationError as DjangoValidationError

from rest_framework import serializers
from rest_framework.exceptions import NotFound
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import HyperlinkedModelSerializer, raise_errors_on_nested_writes

from common.serializers import QueryFieldsMixin
from personas.models import Persona 
from .models import PermisoApp, App, AppPersona, TokenAppInstalar
from .utils import crear_token


class AppTokenSerializer(serializers.ModelSerializer):

    token = SerializerMethodField()
    url = SerializerMethodField()

    def get_token(self, obj):

        t = crear_token(obj.app.persona.uuid,
                           obj.persona.uuid)
        return t

    def get_url(self, obj):
        return obj.app.redirect_url

    class Meta:
        model = App
        fields = ('token','url')


class AppSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    uuid = serializers.UUIDField(read_only=True,source='persona.uuid')

    class Meta:
        model = App
        exclude = ('created','modified','condicion','client_secret',)


class AppInstalarSerializer(serializers.Serializer):

    app = serializers.UUIDField(required=True)
    persona = serializers.UUIDField(required=True)
    tai_instalar = serializers.CharField(required=True,
                                         write_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    #permisos 

    def validate(self, attrs):
        #attrs['user'] = self.context['request'].user
        try:
            tai_instalar = TokenAppInstalar.objects.get(token=attrs['tai_instalar'])
            tai_instalar.delete()
        except Exception as e:
            print (e)
            raise DjangoValidationError("Token invalido")

        return attrs

    def create(self, validated_data):

        u_app  = validated_data.get('app')
        u_per  = validated_data.get('persona')

        app = App.objects.get(persona__uuid=str(u_app))
        persona = Persona.objects.get(uuid=str(u_per))
        ap, c = AppPersona.objects.get_or_create(
                                        persona=persona,
                                        app=app,
                                        user=persona.user)

        for per in app.permisos.all():
            ap.permisos.add(per)

        return ap



class PermisoAppSerializer(QueryFieldsMixin,
                           HyperlinkedModelSerializer):

    
    url = serializers.HyperlinkedIdentityField(view_name="myapp:user-detail")
    
    class Meta:
        model = PermisoApp
        exclude = ('created','modified','condicion')



class JugadorJavascriptSerializer(serializers.Serializer):

    nombre = serializers.CharField(source='primer_nombre')
    apellido = serializers.CharField(source='primer_apellido')
    avatar = SerializerMethodField()
    saldo = SerializerMethodField()


    def get_avatar(self, persona):
        r = self.context['request']
        h = "%s://%s" % ('https' if r.is_secure() else 'http', r.get_host())
        ap = self.context['app_persona']
        per = ap.permisos.filter(codigo='user.get_avatar')
        if not per.exists():
            return None

        try:
            avatar =  h + persona.avatar.url
        except ValueError as e:
            if persona.sexo is None:
                avatar = h + '/media/generic/user.png'
            else:
                if persona.sexo:
                    avatar = h + '/media/generic/hombre.png'
                else:
                    avatar = h + '/media/generic/mujer.png'
        return avatar


    def get_saldo(self, persona):
        ap = self.context['app_persona']
        per = ap.permisos.filter(codigo='user.get_saldo')
        if per.exists():
            return persona.billetera_set.first().total_disponible
        else:
            return None