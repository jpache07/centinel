import hashlib
import uuid as uuid_lib
from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _

from personas.models import Persona, PersonaBaseModel
from common.models import UserRelation, BaseModel


class PermisoApp(BaseModel):

    nombre = models.CharField(_('nombre'), max_length=255)
    codigo = models.CharField(_('codigo'), max_length=100)

    def __str__(self):
        str_r = "{} - {}".format(self.nombre, self.codigo)
        return str_r

    class Meta:
        verbose_name = _('Permiso para App')
        verbose_name_plural = _('Permisos para App')
        ordering = ('id', )


TIPOS_APP = (
    (1,'url'),
    (2,'pc'),
    (3,'nativo'),
)


def generate_client_secret(instance, **k):
    secret = hashlib.sha224(bytes(str(instance.uuid), 'utf8')).hexdigest()
    return secret



def app_directory_path(instance, filename):
    return 'app_{0}/{1}'.format(instance.id, filename)


class App(UserRelation):

    #user aqui es el creador de la app
    nombre = models.CharField(_("nombre"), max_length=65)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    tipo = models.SmallIntegerField(_("tipo"), choices=TIPOS_APP)
    endpoint = models.CharField(_("endpoint"), max_length=965)
    permisos = models.ManyToManyField(
        PermisoApp,
        verbose_name=_('permisos'),
        blank=True,
    )
    instalable = models.BooleanField(_("instalable"), default=True)
    client_secret = models.CharField(
        max_length=255, blank=True, db_index=True
    )

    redirect_url = models.TextField(
        blank=True, help_text=_("URL de retorno"),
    )
    #poster
    poster = models.ImageField(_("Poster"), blank=True, null=True, upload_to=app_directory_path)
    descripcion = models.CharField(max_length=255)

    def save(self, *args, **k):
        if not self.client_secret:
            self.client_secret = generate_client_secret(self.persona)
        super(App, self).save(*args,**k)

    def __str__(self):
        str_r = "{}".format(str(self.nombre))
        return str_r

    # verificar que si es no instalable, no puede tener ningun permiso
    class Meta:
        verbose_name = _('App')
        verbose_name_plural = _('Apps')
        ordering = ('id', )


class TokenAppInstalar(PersonaBaseModel):

    token = models.CharField(
        max_length=255, blank=True, db_index=True
    )
    app = models.ForeignKey(App, on_delete=models.CASCADE)

    def save(self, *args, **k):
        if not self.token:
            self.token = generate_client_secret(self.app.persona)
        super(TokenAppInstalar, self).save(*args,**k)



class AppPersona(UserRelation):

    #user es quien le instalo el app a la persona (el mismo u otro)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    permisos = models.ManyToManyField(
        PermisoApp,
        verbose_name=_('permisos'),
        blank=True,
    )

    def __str__(self):
        return "{} {}".format(str(self.persona),str(self.app))

    class Meta:
        verbose_name = _('App persona')
        verbose_name_plural = _('Apps para persona')
        ordering = ('id', )