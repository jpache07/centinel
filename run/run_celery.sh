#!/bin/sh

# wait for RabbitMQ server to start
sleep 10

cd /code/ahsante
# run Celery worker for our project myproject with Celery configuration stored in Celeryconf
su -m webapp -c "celery worker -A config -Q default -n default@%h"