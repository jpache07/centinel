#!/bin/bash
cd ..
gcc -shared -fPIC stack-fix.c -o stack-fix.so
cd gestionusuarios
export LD_PRELOAD=/code/stack-fix.so
python manage.py migrate
python manage.py runserver 0.0.0.0:8008
