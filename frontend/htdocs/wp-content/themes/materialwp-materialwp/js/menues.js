function templater ( strings, ...keys ) {
  return function( data ) {
      let temp = strings.slice();

      keys.forEach( ( key, i ) => {
          temp[ i ] = temp[ i ] + data[ key ];
      } );

      return temp.join( '' );
  }
};


function extend(obj, src) {
    Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
    return obj;
}


jQuery(document).ready(function($) {
	

	var t_menu = templater`
	<section id="nav_menu" class="widget card widget_nav_menu">
		<div class="menu-logueado-container">
			<ul id="menu-logueado" class="menu">
			</ul>
		</div>
	</section>`;

	var t_menu_normal = templater`
	<li class="menu-item menu-item-type-post_type menu-item-object-page">
	<a href="${ 'url' }">${ 'nombre' }</a></li>`;

	var t_sub_menu = templater`
		<li class="menu-item menu-item-type-taxonomy menu-item-object-page menu-item-has-children">
		<a href="${ 'url' }">${ 'nombre' }</a>
		<ul class="sub-menu">
			${ 'hijos' }		
		</ul>
		</li>`;

	function para_sub_menues(...keys)
	{
		let temp = [];
		keys.forEach(( v, i ) => {
		  	temp.push(v);
		});
		return temp.join( '' );
	}


	$("#secondary").html(t_menu({}));

	$("#menu-logueado").append(t_sub_menu({
		nombre:"Mi perfil", url:"#",
		hijos:para_sub_menues(
			t_menu_normal({url:'/telefonos',nombre:'Telefonos'}),
			t_menu_normal({url:'/termino-condiciones',nombre:'Correo'}),
			t_menu_normal({url:'/termino-condiciones',nombre:'Actividad'})
		)
	}));


	$("#menu-logueado").append(t_menu_normal( {url:'/mis-juegos',nombre:'Mis juegos'} ));
	$("#menu-logueado").append(t_menu_normal( {url:'/mis-aplicaciones',nombre:'Mis aplicaciones'} ));
	$("#menu-logueado").append(t_sub_menu({
		nombre:"Finanzas", url:"#",
		hijos:para_sub_menues(
			t_menu_normal({url:'/bancos',nombre:'Bancos'}),
			t_menu_normal({url:'/centinel',nombre:'Cuenta centinel'})
		)
	}));

	$("#menu-logueado").append(t_sub_menu({
		nombre:"Centinel", url:"#",
		hijos:para_sub_menues(
			t_menu_normal({url:'/acerca-de',nombre:'Acerca de'}),
			t_menu_normal({url:'/termino-condiciones',nombre:'Terminos y condiciones'})
		)
	}));


	if (localStorage.getItem("username") === null) {
	  //... aqui sino tengo el nombre de usuario
	}


	if ($("body").hasClass('page-id-61'))
	{
			
	}
	
})