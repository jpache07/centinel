<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MaterialWP
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
	    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	    	<div class="container">
			   <div class="navbar-brand mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
		   		<div class="collapse navbar-collapse" id="navbarNav">
	            <ul id="menu-navegacion-tope" class="navbar-nav">

	            <li id="menu-item-23" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home active menu-item-23 nav-item"><a href="/tu-perfil/"class="nav-link">tu Perfil</a></li>
				<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-19 current_page_item active menu-item-22 nav-item"><a href="/aplicaciones/"class="nav-link">Aplicaciones</a></li>
				<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-19 current_page_item active menu-item-22 nav-item"><a href="/juegos/"class="nav-link">Juegos</a></li>
				<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-19 current_page_item active menu-item-22 nav-item"><a href="/bots/"class="nav-link">Bots</a></li>

				<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page  page-item-19 current_page_item active menu-item-22 nav-item push-right "><a href="/salir/" class="nav-link">Salir</a></li>											
				</ul>	         
	          </div>

	        </div>
		</nav>
	</header><!-- #masthead -->

	<div id="content" class="site-content">