<?php
/**
 * Enqueue scripts and styles.
 */
function materialwp_scripts() {

	wp_enqueue_style( 'materialwp-material-icons', '//fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons', array(), '0.0.5' );

	wp_enqueue_style( 'materialwp-style', get_stylesheet_directory_uri() . '/style.min.css', array(), '0.0.' );

	//$screen = get_current_screen();

	//print_r($screen);


	wp_enqueue_script( 'materialwp-js', get_template_directory_uri() . '/js/dist/scripts.min.js', array('jquery'), ' ', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


    wp_register_script('miscript_menues', get_stylesheet_directory_uri(). '/js/menues.js', array('jquery'), '1', true );
    wp_enqueue_script('miscript_menues');  
    wp_register_script('centinel_js', get_stylesheet_directory_uri(). '/js/centinel.js', array('jquery'), '1', true );
    wp_enqueue_script('centinel_js');

  
    

}

add_action( 'wp_enqueue_scripts', 'materialwp_scripts' );