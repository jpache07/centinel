<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MaterialWP
 */

?>

	</div><!-- #content -->

	<footer id="colophon" style="bottom: 0; position: absolute; width: 100%; padding: 3rem 0;margin-top: 4rem;background-color: #f8f9fa;margin-bottom: 0;" role="contentinfo">
		<div class="container">
			<div class="site-info">
				&copy; <?php bloginfo( 'name' );
						echo " By Jovan Pacheco";
						echo ' - ';
						echo date("Y"); ?>
			</div><!-- .site-info -->
		</div><!--  .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php 
include_once("variables_javascript.php");
wp_footer(); ?>

</body>
</html>
