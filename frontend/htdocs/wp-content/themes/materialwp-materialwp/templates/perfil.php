<?php
/**
 * Template Name: Perfil
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MaterialWP
 */


if(!usuario_login_centinel()) : 
	wp_redirect(site_url('/login/').'?redirect_to=' . $_SERVER["REQUEST_URI"]); 
endif; 

get_header('logueado'); ?>


	<div class="container">
		<div class="row">

			<aside id="secondary" class="widget-area" role="complementary">
				<?php #dynamic_sidebar( 'sidebar-1' ); ?>
			</aside><!-- #secondary -->

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<article id="post-<?php the_ID(); ?>" <?php post_class('card'); ?>>

						<div class="card-body">
							<header class="entry-header">
								<h2 class="entry-title">Tu perfil</h2>
							</header><!-- .entry-header -->

							<div class="entry-content">
								fds
							</div><!-- .entry-content -->

						</div><!--  .card-body -->
					</article><!-- #post-## -->

				</main><!-- #main -->
			</div><!-- #primary -->

	</div><!--  .row -->

</div><!--  .container -->

<?php
get_footer('dos');
