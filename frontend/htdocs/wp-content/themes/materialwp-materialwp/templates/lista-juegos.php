<?php
/**
 * Template Name: Lista de juegos
 *
 * Displays a full width page without a sidebar.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MaterialWP
 */


if(!usuario_login_centinel()) : 
	wp_redirect(site_url('/login/').'?redirect_to=' . $_SERVER["REQUEST_URI"]); 
endif; 

get_header('logueado'); 

try {

	function curl_get_contents($url)
	{
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	  $data = curl_exec($ch);
	  if (curl_exec($ch) === FALSE) { die("Curl Failed: " . curl_error($ch)); } else { return curl_exec($ch); } 
	  curl_close($ch);
	  return $data;
	}

	$content = curl_get_contents('http://192.168.1.213:9001/v1/apps/');

	
	//print_r($content);
    if ($content === false) {
        
    }
    else
    {
	$juegos = json_decode($content, true);
	?>   

	<div class="container">
		<div class="row">
			<div id="primary" class="content-area-full">
				<main id="main" class="site-main" role="main">

					<article id="post-<?php the_ID(); ?>" <?php post_class('card'); ?>>

						<div class="card-body">
							<header class="entry-header">
								<h2 class="entry-title">Todos los juegos</h2>
							</header><!-- .entry-header -->

							<div class="entry-content">

<div class="row">
 	<?php foreach ($juegos as $key => $v) { 
 		$url = "http://192.168.1.213:9001/v1/aplications/".$v['uuid']."/".$_COOKIE['centinel_uuid'] . "/";
 		?>
	  <div class="col-sm-6 col-md-4">
	    <div class="thumbnail">
	      <img src="<?=$v['poster']?>" alt="img">
	      <div class="caption">
	        <h3><?=$v['nombre']?></h3>
	        <p>fdsfds</p>
	        <p>
	        <form action="<?=$url?>" method='POST' target="_black">
	        	<input type="hidden" name='ut' value="<?=$_COOKIE['centinel_token']?>"></input>
	        	<button class="btn btn-primary btn-success btn-raised" role="button">Ir a jugar</button>

	        <a href="#" class="btn btn-primary btn-info btn-raised" role="button">Visitar</a>
	        </form>
	        </p>
	      </div>
	    </div>
	  </div> 		
 	<?php } ?>



</div>								


							</div><!-- .entry-content -->

						</div><!--  .card-body -->
					</article><!-- #post-## -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- row -->
	</div><!-- container -->

	<?php 	

    }
} catch (Exception $e) {
    print_r($e);
}


get_footer();
