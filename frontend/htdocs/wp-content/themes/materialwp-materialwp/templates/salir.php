<?php
/**
 * Template Name: Cerrar sesion
 *
 *
 * @package MaterialWP
 */


if (isset($_COOKIE['centinel_token'])) {
    unset($_COOKIE['centinel_token']);
    unset($_COOKIE['centinel_uuid']);
    setcookie('centinel_token', '', time() - 3600, '/'); // empty value and old timestamp
}
wp_redirect(site_url('/login')); 

