<?php
/**
 * Template Name: Bancos
 *
 * Pagina que lista los bancos
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MaterialWP
 */


if(!usuario_login_centinel()) : 
	wp_redirect(site_url('/login/').'?redirect_to=' . $_SERVER["REQUEST_URI"]); 
endif; 

get_header('logueado'); ?>

	<div class="container">
		<div class="row">

			<aside id="secondary" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</aside><!-- #secondary -->

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

				</main><!-- #main -->
			</div><!-- #primary -->

	</div><!--  .row -->
</div><!--  .container -->


<?php
get_footer('dos');


