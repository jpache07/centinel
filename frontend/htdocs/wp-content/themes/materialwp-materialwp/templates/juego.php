<?php
/**
 * Template Name: Juego
 *
 * Displays a full width page without a sidebar.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MaterialWP
 */


if(!usuario_login_centinel()) : 
	wp_redirect(site_url('/login/').'?redirect_to=' . $_SERVER["REQUEST_URI"]); 
endif; 

$juegos = array(

	'dfdsfd' => array('url' => '/juegos-local/prueba/index.php','titulo'=>'Mariana antuan 1' ),
	'548545' => array('url' => '/juegos-local/prueba/index.php','titulo'=>'Mariana antuan 2' ),
	'5gfhhfghfgh' => array('url' => '/juegos-local/plataforma/index.php','titulo'=>'Plataforma imagen' )

	);

$url_cargar = $juegos[$_GET['key']]['url'] . '?jugador=' . $_COOKIE['centinel_uuid'];
get_header('logueado');
?>

	<div class="container">
		<div class="row">
			<div id="primary" class="content-area-full">
				<main id="main" class="site-main" role="main">

					<article id="post-<?php the_ID(); ?>" <?php post_class('card'); ?>>

						<div class="card-body">
							<header class="entry-header">
								<h2 class="entry-title"><?=$juegos[$_GET['key']]['titulo']?></h2>
							</header><!-- .entry-header -->
							<div class="entry-content">
								<iframe src="<?=$url_cargar?>" width="100%" height="500px;" allowfullscreen>>
							</div><!-- .entry-content -->

						</div><!--  .card-body -->
					</article><!-- #post-## -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div><!-- row -->
	</div><!-- container -->

<?php
get_footer();
