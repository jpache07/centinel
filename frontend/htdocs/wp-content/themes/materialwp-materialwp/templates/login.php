<?php
/**
 * Template Name: Login
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package materialwp
 */

if(usuario_login_centinel()) : 
  wp_redirect(site_url('/tu-perfil/')); 
endif; 

get_header(); ?>


<div class="columns">
<div class="container">
	<h2>Todo lo que podemos hacer contigo. </h2>

	<p>Obten beneficios en juegos pagando desde tú cuenta, recarga saldo a tus celulares, envia dinero a amigos, etc</p>

</div>
</div>


<div class="row justify-content-center mt-1">
<!-- Nav tabs -->
  <ul class="nav nav-pills">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#login">Ingreso</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#registro">Registro</a>
    </li>
  </ul>
<!-- Tab panes -->
</div>


<div class="row justify-content-center mt-1">

  <div class="tab-content col-xs-6 col-sd-6 col-md-4">
    <div class="tab-pane active" id="login">
      <div class="card" id="login">
          <form class="form" id="formulario_ingreso">

            <div class="card-header card-header-primary text-center">
              <h4 class="card-title">Ingresa</h4>
              <div class="social-line">
                <a href="#pablo" class="btn btn-just-icon btn-link">
                  <i class="fa fa-facebook-square"></i>
                </a>
                <a href="#pablo" class="btn btn-just-icon btn-link">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="#pablo" class="btn btn-just-icon btn-link">
                  <i class="fa fa-google-plus"></i>
                </a>
              </div>
            </div>
            <div class="card-body">

              <span class="bmd-form-group"><div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">mail</i>
                  </span>
                </div>
                <input class="form-control" placeholder="Correo" type="email" id="email_ingreso" name="username">
              </div></span>

              <span class="bmd-form-group"><div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                      <i class="material-icons">lock_outline</i>
                  </span>
                </div>

                <input class="form-control" placeholder="Clave." type="password" name="password">
              </div></span>


            </div>
            <div class="footer text-center">
              <a id="btn_login" class="btn btn-primary btn-success btn-raised btn-lg">Entrar.</a>
            </div>
          </form>
      </div>    
    </div>
    <div class="tab-pane fade" id="registro">
      <div class="card">
        <form class="form" method="" action="">

          <div class="card-header card-header-primary text-center">
            <h4 class="card-title">Registrate</h4>
            <div class="social-line">
              <a href="#pablo" class="btn btn-just-icon btn-link">
                <i class="fa fa-facebook-square"></i>
              </a>
              <a href="#pablo" class="btn btn-just-icon btn-link">
                <i class="fa fa-twitter"></i>
              </a>
              <a href="#pablo" class="btn btn-just-icon btn-link">
                <i class="fa fa-google-plus"></i>
              </a>
            </div>
          </div>
          <p class="description text-center"></p>
          <div class="card-body">

            <span class="bmd-form-group">
              <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">face</i>
                </span>
              </div>
              <input class="form-control" placeholder="Nombre" type="text" id="primer_nombre">
              <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">face</i>
                </span>
              </div>
              <input class="form-control" placeholder="Apellido" type="text" id="primer_apellido">
              </div>
            </span>

            <span class="bmd-form-group"><div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">mail</i>
                </span>
              </div>
              <input class="form-control" placeholder="Correo" type="email" id="correo_electronico">
            </div></span>

            <span class="bmd-form-group"><div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                </span>
              </div>

              <input class="form-control" placeholder="Clave" type="password" id="clave_usuario">
            </div></span>

            <!-- If you want to add a checkbox to this form, uncomment this code

            <div class="checkbox">
              <label>
                <input type="checkbox" name="optionsCheckboxes" checked>
                Subscribe to newsletter
              </label>
            </div> -->
          </div>
          <div class="footer text-center">
            <a id="btn_registro" class="btn btn-primary btn-success btn-raised btn-lg">Registrate.</a>
          </div>
        </form>
      </div>    
    </div>      
  </div>

</div>
<?php get_footer(); ?>
