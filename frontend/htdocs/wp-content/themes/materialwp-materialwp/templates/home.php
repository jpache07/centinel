<?php
/**
 * Template Name: Home
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package materialwp
 */

if(usuario_login_centinel()) : 
  wp_redirect(site_url('/tu-perfil/')); 
endif; 

get_header(); ?>

<div class="home-hero">
	<div class="container">
		<h1>Centinel</h1>
		<p class="lead">La plataforma para tú uso diario.</p>
		<p><a href="/login" class="btn btn-primary btn-success btn-raised btn-lg">Ingrese con nosotros</a> </p>
		
	</div>
</div>

<div class="columns">
<div class="container">
	<h2>Todo lo que podemos hacer contigo. </h2>

	<p>Obten beneficios en juegos pagando desde tú cuenta, recarga saldo a tus celulares, enviando dinero a amigos, etc</p>


	<div class="row">
		<div class="col-md-4 col-lg-4">
			
            <i class="material-icons">apps</i>

			<h3>Aplicaciones </h3>
			<p>Disponemos de aplicaciones que facilitan tú interación con muchas plataformas, desde consultar tu cuenta del Venezuela, hasta hacer recargas a Digitel y Movistar.</p>
		</div>

		<div class="col-md-4 col-lg-4">
			<i class="material-icons">view_day</i>
			
			<h3>Mensajes</h3>
			<p>Puedes chatear con operadores que deseas que realicen un trabajo por ti, o con un programa automatico.  </p>
		</div>

		<div class="col-md-4 col-lg-4">
			
			<i class="material-icons">view_carousel</i>
			<h3>Juegos</h3>
			<p>Tenemos decenas de juegos, desde juegos clasicos hasta juego 100% hechos en Venezuela y autoptonos de tú zona.</p>
		</div>
	</div>

	<p><a href="/centinel" class="btn btn-default btn-raised btn-lg"> Saber m&aacute;s</a> </p>

	</div>
</div>
<?php get_footer(); ?>
