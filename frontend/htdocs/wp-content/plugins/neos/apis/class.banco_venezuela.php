<?php
/*
Api para el Banco de Venezuela
*/

class BancoVenezuela {

	static $url_auth = '';
	static $url_delete = '';
	static $navigator = '';
	static $saldos = '';
	static $host = '';
	static $base_url = '';


	public static function init() {

		self::$host 		= 'bdvenlinea.banvenez.com';
		self::$base_url 	= "https://".self::$host."/";
		self::$url_auth 	= self::$base_url . "identity/oauth/token?grant_type=password&username=%s&password=%s";
		self::$url_delete 	= self::$base_url . "identity/oauth/token";
		self::$saldos 		= self::$base_url . 'consultasaldocuenta/consultaSaldoCuenta';
		self::$navigator 	= 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0';
		
	}


	public static function _token( $user , $pass) {

		self::init();
		$url = sprintf(self::$url_auth , $user , $pass);
 		$args = array(
 			'method' => 'POST',
			'headers' => array(
			  	'User-agent' => self::$navigator,
				'Host'=>self::$host,
				'Content-Type'=> 'application/json'				  	
		));

		$response_banco = wp_remote_request( $url, $args );
		//var_dump($response_banco);
		if ($response_banco['response']['code'] == 200)
		{
			return json_decode($response_banco['body'], true);
		}
		else
		{
			return json_decode($response_banco['body'], true)['error_description'];
		}

	}


	public static function get_token( $request ) {

		$user = $request->get_param('username');
		$pass = $request->get_param('password');
		$response = self::_token($user, $pass);
		$response_api = array('access_token' => $response['access_token'] );
		return rest_ensure_response( $response_api );

	}


	public static function _saldo_cuentas_token( $token) {

		self::init();
		$args = array(
			'method' => 'GET',
			'headers' => array(
			  	'User-agent' => self::$navigator,
			  	'Authorization' => 'Bearer '. $token,
				'Host'=>self::$host,
				'Content-Type'=> 'application/json'				  	
				)
			);

		$response_banco = wp_remote_request( self::$saldos, $args );
		if ($response_banco['response']['code'] == 200)
		{
			return json_decode($response_banco['body'], true);
		}
		else
		{
			return json_decode($response_banco['body'], true)['error_description'];
		}

	}

	public static function _saldo_cuentas( $user , $pass) {

		$response_token = self::_token($user, $pass);
		if (is_array($response_token))
		{			
 			return self::_saldo_cuentas_token($response_token['access_token']);
		}
		else
		{
			return $response_token;
		}

	}

	private static function get_saldo_cuentas( $request ) {

		$user = $request->get_param('username');
		$pass = $request->get_param('password');
		$response = self::_token($user, $pass);
		$token = $response['access_token'];
 		$args = array(
 				'method' => 'GET',
				'headers' => array(
				  	'User-agent' => self::$navigator,
				  	'Authorization' => 'Bearer '. $token,
					'Host'=>self::$host,
					'Content-Type'=> 'application/json'				  	
					)
 				);
		
		$response_banco = wp_remote_request( self::$saldos, $args );
		$response_banco_json = json_decode($response_banco['body'],true);
		return rest_ensure_response( $response_banco_json );

	}


	public static function delete_token( $request ) {

		self::init();
		$user = $request->get_param('username');
		$pass = $request->get_param('password');

		$url = sprintf(self::$url_auth , $user , $pass);
 		$args = array('method' => 'POST');
		
		$response_banco = wp_remote_request( $url, $args );
		$response_banco_json = json_decode($response_banco['body'],true);
		//var_dump($response_banco_json['error']);
		$response_api = array('token' => $response_banco_json['access_token'] );

		return rest_ensure_response( $response_api );

	}


	public static function sanitize_user($campo, $request, $param)
	{
		$valor = trim( $campo );
		return $valor;
	}


}

