<?php
/*
Plugin Name: Neos
Plugin URI: 
Description: Plugin para centralizar apis.
Author: Jovan Pacheco
Author URI: 
Version: 0.1
License: GPLv2
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

define( 'NEOS__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( NEOS__PLUGIN_DIR . 'class.neos_pages.php' );
require_once( NEOS__PLUGIN_DIR . 'apis/class.banco_venezuela.php' );
require_once( NEOS__PLUGIN_DIR . 'class.neos_api.php' );
require_once( NEOS__PLUGIN_DIR . 'class.neos.php' );

// Hooks
register_activation_hook( __FILE__ , array('NeosPlugin','activate' ));
register_deactivation_hook( __FILE__ , array('NeosPlugin','deactivate' ));

//add_action('admin_menu',array('NeosPlugin','divisas_menues'));
//add_action('admin_enqueue_scripts', array('DivisasPlugin','load_custom_wp_admin_style' ));
//add_action('init', array('DivisasPlugin','wpse_enqueue_datepicker' ));
add_action('rest_api_init', array( 'NeosApiRest', 'init' ) );
//add_action('wp_ajax_divisa_ajax', array('DivisasPlugin','ajax_cotizaciones' ) );
//add_action('wp_ajax_nopriv_divisa_ajax', array('DivisasPlugin','ajax_cotizaciones' ) );


add_filter('json_enabled', '__return_true');
add_filter('json_jsonp_enabled', '__return_true');

?>
