<?php

class NeosApiRest {

	/**
	 * Register the REST API routes.
	 */
	public static function init() {

		if ( ! function_exists( 'register_rest_route' ) ) {
			// The REST API wasn't integrated into core until 4.4, and we support 4.0+ (for now).
			return false;
		}

		register_rest_route( 'neos', 'get', array(
			'methods' => WP_REST_Server::READABLE,
			'callback' => array( 'NeosApiRest', 'get' )
		));

		register_rest_route( 'neos/venezuela', 'token', array(
			'methods' => WP_REST_Server::CREATABLE,
			'callback' => array( 'BancoVenezuela', 'get_token' ),
			'args' => array(
				'username' => array(
					'required' => true,
					'type' => 'string',
					'sanitize_callback' => array( 'BancoVenezuela', 'sanitize_user' ),
					'description' => 'Campo para el usuario que viene siendo la tarjeta de debido',
				),
				'password' => array(
					'required' => true,
					'type' => 'string',
					'sanitize_callback' => array( 'BancoVenezuela', 'sanitize_user' ),
					'description' => 'Campo para la clave.o',
				),				
			),
		));

		register_rest_route( 'neos/venezuela', 'saldo_cuentas', array(
			'methods' => WP_REST_Server::CREATABLE,
			'callback' => array( 'BancoVenezuela', 'get_saldo_cuentas' ),
			'args' => array(
				'username' => array(
					'required' => true,
					'type' => 'string',
					'sanitize_callback' => array( 'BancoVenezuela', 'sanitize_user' ),
					'description' => 'Campo para el usuario que viene siendo la tarjeta de debido',
				),
				'password' => array(
					'required' => true,
					'type' => 'string',
					'sanitize_callback' => array( 'BancoVenezuela', 'sanitize_user' ),
					'description' => 'Campo para la clave.o',
				),				
			),
		));

	}

	public static function get( $request ) {
		$response = array();
		return rest_ensure_response( $response );
	}
	
}