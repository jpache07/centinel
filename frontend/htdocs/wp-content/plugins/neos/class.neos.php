<?php

class NeosPlugin {

	public static function divisas_menues() {

	    add_menu_page('Neos Plugin',
	                  'Neos Plugin',
	                  'manage_options',
	                  'pagina_uno', array('NeosPages','pagina_uno')
	                  ); 
	}


	public static function activate() {
	}


	public static function deactivate() {
	}


	public static function load_custom_wp_admin_style() {

	    $screen = get_current_screen();
	    if ($screen->base != "dolar_page_page_dolar_page" and
	    	$screen->base != "toplevel_page_dolar_page" and
	    	$screen->base != "divisas-dolar_page_euro_page")
	        return;


	    $dir = plugin_dir_url(__FILE__);
	    wp_enqueue_script("jquery");
	    wp_register_style( 'custom_wp_admin_css', $dir.'css/style.css', false, '1.0.0' );
	    wp_enqueue_style( 'custom_wp_admin_css' );
	    wp_register_style( 'boostrap_css', $dir.'css/bootstrap.min.css', false, '1.0.0' );
	    wp_enqueue_style( 'boostrap_css' );
	    wp_register_script('divisas_js', $dir.'js/script.js', array('jquery'), '1.3', true );
	    wp_enqueue_script('divisas_js');
	    wp_register_script('sweetalert', $dir . 'js/sweetalert.min.js');
	    wp_enqueue_script('sweetalert');
	}


	public static function wpse_enqueue_datepicker() {
		if (!empty($_GET['page']))
		{
			if ($_GET['page']=='dolar_page' ||
				$_GET['page']=='euro_page')
			{
			wp_enqueue_script('jquery-ui-datepicker');
		    wp_enqueue_style( 'jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/flick/jquery-ui.css');
			}
		}
	}


	public static function ajax_cotizaciones() { 

		if ('ajax' == $_POST['action'])
		{
			$fecha = explode('-', $_POST['fecha']);
			$nf = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
			$resp = DivisasPlugin::get_cotizaciones_fecha($nf , $_POST['moneda_id']);   
	    	file_put_contents( __DIR__ . '/my_loggg.txt', ob_get_contents() );

	    	$response = array();
			foreach ($resp as $key => $value) {
			
				$response[] =  array(
					'moneda'=>$value->moneda,
					'fuente'=>$value->fuente,
					'valor'=>number_format($value->valor, 2, ',', '.'),
					'fecha'=>$value->fecha,
					'hora'=>$value->hora
				);

			}
	    	wp_send_json($response);
		}
		
	}

}