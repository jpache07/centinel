<?php
/*
Plugin Name: Jovan
Plugin URI: 
Description: Gui para centralizar apis.
Author: Jovan Pacheco
Author URI: 
Version: 0.1
License: GPLv2
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

define( 'JOVAN__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( JOVAN__PLUGIN_DIR . 'class.jovan_pages.php' );
require_once( JOVAN__PLUGIN_DIR . 'class.jovan.php' );

// Hooks
register_activation_hook( __FILE__ , array('JovanPlugin','activate' ));
register_deactivation_hook( __FILE__ , array('JovanPlugin','deactivate' ));

add_action('admin_menu',array('JovanPlugin','divisas_menues'));
//add_action('admin_enqueue_scripts', array('DivisasPlugin','load_custom_wp_admin_style' ));
//add_action('init', array('DivisasPlugin','wpse_enqueue_datepicker' ));
//add_action('wp_ajax_divisa_ajax', array('DivisasPlugin','ajax_cotizaciones' ) );
//add_action('wp_ajax_nopriv_divisa_ajax', array('DivisasPlugin','ajax_cotizaciones' ) );


?>
