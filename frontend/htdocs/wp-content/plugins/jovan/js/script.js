function eliminar(id) {

	swal({
	  title: "Eliminar cotización",
	  text: "¿Estas seguro que desea eliminar esta cotización?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((willChange) => {
	  if (willChange) {
		jQuery("#_action").val('delete-divisa');
		jQuery("#_id").val(id);
		jQuery("#_fecha").val(jQuery("#fecha").val());
		jQuery("#form_action").submit();
	  }
	  else
	  {
	    swal("Su cotización no fue eliminada.");
	  }
	});
}


function agregar(id) {

	if (jQuery("#fecha").val() == '' ||
		jQuery("#hora").val() == '' ||
		jQuery("#fuente").val() == '' ||
		jQuery("#valor").val() == '')
	{
		swal({
		  title: "Agregar cotización",
		  text: "Por favor verifique los datos e intente nuevamente",
		  icon: "warning",
		  dangerMode: false,
		});
	}
	else
	{
		jQuery("#from_nueva_cotizacion").submit();
	}

}

function modificar() {

	if (jQuery("#fecha").val() == '' ||
		jQuery("#hora").val() == '' ||
		jQuery("#fuente").val() == '' ||
		jQuery("#valor").val() == '')
	{
		swal({
		  title: "Modifcar cotización",
		  text: "Por favor verifique los datos e intente nuevamente.",
		  icon: "warning",
		  dangerMode: false,
		});
	}
	else
	{
		jQuery("#id_modificar").val(localStorage.getItem('cotizacion_id'));	
		jQuery("#action_cotizacion").val('edit-divisa');	
		jQuery("#from_nueva_cotizacion").submit();
	}

}


function modificar_action(self,id) {
	var tds = jQuery(self).parent().siblings('td');
	jQuery("#hora").val(tds[0].innerHTML);
	var val = tds[1].innerHTML;;
	var val = val.replace('.','');
	var val = val.replace('.','');
	var val = val.replace(',','.');
	var val = parseFloat(val);
	jQuery("#valor").val(val);
	jQuery("#fuente").val(tds[2].innerHTML);
	jQuery("#btn_modificar").removeClass('d-none');
	jQuery("#btn_agregar").addClass('d-none');
	localStorage.setItem('cotizacion_id',id);
}


function limpiar_form() {
	jQuery("#btn_agregar").removeClass('d-none');
	jQuery("#btn_modificar").addClass('d-none');	
	jQuery('#from_nueva_cotizacion').trigger("reset");
}


jQuery(document).ready(function($) {
    
    $("#fecha").datepicker({
        dateFormat : "dd-mm-yy"
    });

    $("#fecha").on("change",function(){
    	//alert($("#fecha").val());
		var target = '/wp-admin/admin-ajax.php' ;
		var data = {
		    action: 'divisa_ajax',
		    fecha: $("#fecha").val(),
		    moneda_id: $("#moneda_id").val()
		};
		var req = jQuery.post( target , data,  function(resp) { 
			$("#tabla_cotizaciones > tbody").html(""); 

			for (var i = 0; i < resp.length; i++) {
				var row = '';
				var o = resp[i];
				row += "<tr id='"+o.id+"'>";
            	row += "<td>"+o.hora+"</td>";
            	row += "<td>"+o.valor+"</td>";
            	row += "<td>"+o.fuente+"</td>";
            	row += "<td><button type=\"button\" class=\"btn btn-info\" onclick=\"modificar_action(this,'"+o.id+"')\">Modificar</button>&nbsp;";
            	row += "<button type=\"button\" class=\"btn btn-danger\" onclick=\"eliminar('"+o.id+"')\">Eliminar</button>";
            	row += "</td></tr>";
            	$("#tabla_cotizaciones").append(row);
			}
			
		}) ;    
		    	
    })
});