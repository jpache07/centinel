var http = require('http').createServer(handleServeHttp);
var io = require('socket.io')(http);
var redis = require('redis');
var request = require('request');
var fs = require('fs');
require('events').EventEmitter.prototype._maxListeners = 1000;


var redisClient = redis.createClient(6379, 'redis');
var connectedClients = [];
var userList = [];
var redisSends = [];


redisClient.on("error", function (err) {
    console.log(err);
});


http.listen(3000, function () {
    console.log('Listening on port 3000.');
});


io.on('connection', function (socket) {
    console.log('A user has connected...' + socket.id);
    connectedClients[socket.id] = {};
    connectedClients[socket.id].socket = socket;
    connectedClients[socket.id].emited = [];

    socket.on("disconnect", function (s) {
        disconnectUser(socket);
    });


    socket.on('req_disconnect', function () {
        disconnectUser(socket);
    });

    socket.join('public');
    redisClient.subscribe('public');

    redisClient.on('message', function (channel, message) {
        console.log('channel   message   socketid');
        console.log(channel+' '+message+' '+socket.id)
        //data_json = JSON.parse(message);
        //string = channel + data_json.task_id;

        //if (!(redisSends.includes(string))) {
            //redisSends.push(string);
            sendResult(socket, channel, message);
        //}

    });

});




function sendResult(socket, channel, message) {

    console.log('In sendResult')
    data = JSON.parse(message)
    if (data.success) {

        if (data.result_type == 'message') {
            console.log('En send message ')
            io.sockets.in(data.uuid).emit('result', message);
        }// end message

        if (data.result_type == 'chat_start') {
            console.log('chat_start')
            socket.join(data.uuid);
            socket.emit('result', message);
        }// end chat_start

        if (data.result_type == 'public'){
            io.sockets.in('public').emit('ev_public', message);
        }// end public

    }
    else
    {
        console.log('Error in result' + message)
    }

}// end sendresult


function startChat(socket, data) {

    request({
        url: 'http://django:8008/api/v1/messaging/chat/start/',
        method: 'POST',
        jar: true,
        formData: {
            to: data.to,
        },
        headers: {
            'Authorization': 'jwt ' + data.jwt
        }
    }, function (error, response, body) {
        if (!error && response.statusCode == 200) {

            redisClient.subscribe(body);
            console.log('task > ' + body)

        } else {
            error_request(error, body)
        }
    });
}

function sendMessage(socket, msg) {

    request({
        url: 'http://django:8008/api/v1/messaging/messages/send/',
        method: 'POST',
        //jar : true,
        formData: {
            message_body: msg.message_body,
            uuid: msg.uuid
        },
        headers: {
            'Authorization': 'jwt ' + msg.jwt
        }
    }, function (error, response, body) {

        if (!error && response.statusCode == 201) {
            info = JSON.parse(body)
            redisClient.subscribe(info.task_id);
            redisClient.subscribe(body);
            console.log('task sendMessage > ' + info.task_id)

        } else {
            error_request(error, body)
        }
    });

}


function error_request(error, body) {
    console.log(error);
    console.log(body);
    var stream = fs.createWriteStream("error_socketio.html");
    stream.once('open', function (fd) {
        stream.write(body);
        stream.end();
    });
}


function handleServeHttp(request, response) {

    if (request.url == "/example") {
        response.end("response");
    }
}


function disconnectUser(socket) {
    if (typeof(connectedClients[socket.id]) == "undefined" ||
        typeof(connectedClients[socket.id].userName) == "undefined") {
        return;
    }
    if (connectedClients.hasOwnProperty(socket.id)) {
        delete connectedClients[socket.id];
    }
    console.log('disconnect 1 ');
}